# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171113035206) do

  create_table "activities", force: true do |t|
    t.integer  "user_id"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.string   "action"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["resource_id", "resource_type"], name: "index_activities_on_resource_id_and_resource_type", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "authentications", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authentications", ["user_id"], name: "index_authentications_on_user_id", using: :btree

  create_table "bonus", force: true do |t|
    t.integer  "user_id"
    t.decimal  "amount",     precision: 11, scale: 2, default: 0.0
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bonus", ["user_id"], name: "index_bonus_on_user_id", using: :btree

  create_table "bulk_votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "pageant_id"
    t.integer  "contestant_id"
    t.integer  "premium_transaction_id"
    t.integer  "quantity",               default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bulk_votes", ["contestant_id"], name: "index_bulk_votes_on_contestant_id", using: :btree
  add_index "bulk_votes", ["pageant_id"], name: "index_bulk_votes_on_pageant_id", using: :btree
  add_index "bulk_votes", ["premium_transaction_id"], name: "index_bulk_votes_on_premium_transaction_id", using: :btree
  add_index "bulk_votes", ["user_id"], name: "index_bulk_votes_on_user_id", using: :btree

  create_table "bundles", force: true do |t|
    t.integer  "quantity"
    t.decimal  "price",      precision: 11, scale: 2, default: 0.0
    t.decimal  "share",      precision: 11, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contents", force: true do |t|
    t.string   "area"
    t.string   "title"
    t.text     "script"
    t.integer  "status",      default: 0
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
  end

  create_table "contestants", force: true do |t|
    t.integer  "pageant_id"
    t.integer  "number"
    t.string   "name"
    t.text     "description"
    t.integer  "gender",      default: 0
    t.integer  "status",      default: 0
    t.integer  "vote_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "votes_count", default: 0, null: false
  end

  add_index "contestants", ["pageant_id"], name: "index_contestants_on_pageant_id", using: :btree

  create_table "pageants", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.integer  "category",            default: 0
    t.datetime "voting_start"
    t.datetime "voting_end"
    t.boolean  "show_votes",          default: true
    t.boolean  "boost_votes",         default: false
    t.integer  "status",              default: 0
    t.text     "fb_button"
    t.string   "header_file_name"
    t.string   "header_content_type"
    t.integer  "header_file_size"
    t.datetime "header_updated_at"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.datetime "thumb_updated_at"
    t.integer  "thumb_file_size"
    t.string   "thumb_content_type"
    t.string   "thumb_file_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pageants", ["user_id"], name: "index_pageants_on_user_id", using: :btree

  create_table "photos", force: true do |t|
    t.integer  "contestant_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "photos", ["contestant_id"], name: "index_photos_on_contestant_id", using: :btree

  create_table "premium_denominations", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.text     "description"
    t.decimal  "price",       precision: 11, scale: 2, default: 0.0
    t.integer  "validity",                             default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "premium_transactions", force: true do |t|
    t.integer  "premium_denomination_id"
    t.integer  "user_id"
    t.decimal  "price",                   precision: 11, scale: 2, default: 0.0
    t.integer  "quantity",                                         default: 1
    t.integer  "status",                                           default: 0
    t.text     "token"
    t.string   "payer_id"
    t.string   "txn_id"
    t.datetime "completed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "premium_transactions", ["premium_denomination_id"], name: "index_premium_transactions_on_premium_denomination_id", using: :btree
  add_index "premium_transactions", ["user_id"], name: "index_premium_transactions_on_user_id", using: :btree

  create_table "settings", force: true do |t|
    t.string   "title"
    t.string   "key"
    t.text     "value",      limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "blocked",          default: false
    t.integer  "gender",           default: 0
    t.date     "birth_date"
    t.string   "location"
    t.string   "paypal_email"
    t.string   "url"
    t.integer  "role",             default: 0
    t.date     "premium_until"
    t.text     "auth"
    t.text     "token"
    t.datetime "token_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "votes_count",      default: 0,     null: false
    t.integer  "pageants_count",   default: 0,     null: false
  end

  create_table "votes", force: true do |t|
    t.integer  "contestant_id"
    t.integer  "user_id"
    t.boolean  "bought",        default: false
    t.integer  "quantity",      default: 1
    t.string   "ip_address"
    t.string   "platform"
    t.string   "browser"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["contestant_id"], name: "index_votes_on_contestant_id", using: :btree
  add_index "votes", ["user_id"], name: "index_votes_on_user_id", using: :btree

  create_table "wallet_logs", force: true do |t|
    t.integer  "wallet_id"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.decimal  "amount",        precision: 11, scale: 2, default: 0.0
    t.text     "note"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wallet_logs", ["resource_id", "resource_type"], name: "index_wallet_logs_on_resource_id_and_resource_type", using: :btree
  add_index "wallet_logs", ["wallet_id"], name: "index_wallet_logs_on_wallet_id", using: :btree

  create_table "wallets", force: true do |t|
    t.integer  "user_id"
    t.decimal  "amount",     precision: 11, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "wallets", ["user_id"], name: "index_wallets_on_user_id", using: :btree

  create_table "withdrawals", force: true do |t|
    t.integer  "user_id"
    t.string   "txn_id"
    t.decimal  "amount",     precision: 11, scale: 2, default: 0.0
    t.integer  "status",                              default: 0
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "withdrawals", ["user_id"], name: "index_withdrawals_on_user_id", using: :btree

  create_table "wysiwyg_images", force: true do |t|
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
