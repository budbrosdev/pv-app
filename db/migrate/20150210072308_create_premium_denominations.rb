class CreatePremiumDenominations < ActiveRecord::Migration
  def change
    create_table :premium_denominations do |t|
      t.string :code
      t.string :name
      t.decimal :price, precision: 11, scale: 2, default: 0
      t.integer :validity, default: 0

      t.timestamps
    end
    
    PremiumDenomination.create(code: "PVP-30D", name: "Pageant Vote Premium (30days)", price: 1.99, validity: 30)
    PremiumDenomination.create(code: "PVP-TRIAL", name: "Pageant Vote Premium (trial)", price: 0, validity: 1)
  end
end
