class CreateBulkVotes < ActiveRecord::Migration
  def change
    create_table :bulk_votes do |t|
      t.references :user, index: true
      t.references :pageant, index: true
      t.references :contestant, index: true
      t.references :premium_transaction, index: true

      t.timestamps
    end
  end
end
