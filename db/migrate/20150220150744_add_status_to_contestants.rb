class AddStatusToContestants < ActiveRecord::Migration
  def change
    add_column :contestants, :status, :integer, after: "gender", default: "0"
  end
end
