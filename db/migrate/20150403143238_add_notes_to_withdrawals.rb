class AddNotesToWithdrawals < ActiveRecord::Migration
  def change
    add_column :withdrawals, :notes, :text, after: "status"
  end
end
