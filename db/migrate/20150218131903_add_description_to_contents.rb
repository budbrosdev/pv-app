class AddDescriptionToContents < ActiveRecord::Migration
  def change
    add_column :contents, :description, :text

    content = Content.find_by(area: "create-a-pageant")
    content.update_attributes(description: "Need help on creating your pageant?") if content.present?

    content = Content.find_by(area: "premium-membership")
    content.update_attributes(description: "Help and support your favourite contestants by being able to vote multiple times daily.") if content.present?

    content = Content.find_by(area: "how-to-vote")
    content.update_attributes(description: "Find out how you can simply support your favourite contestants.") if content.present?

    content = Content.find_by(area: "faq")
    content.update_attributes(description: "Have a question and need a quick response? Find answers now through our Frequently Asked Questions page.", title: "Frequently Asked Questions") if content.present?

    content = Content.find_by(area: "contact-us")
    content.update_attributes(description: "Feel free to reach us.") if content.present?

    content = Content.find_by(area: "what-is-autolike")
    content.update_attributes(description: "Find out more on what an auto-like is and how Pageant Vote can help you.") if content.present?
  end
end
