class AddPremiumUntilToUsers < ActiveRecord::Migration
  def change
    add_column :users, :premium_until, :date, after: "role"
  end
end
