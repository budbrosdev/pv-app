class AddTokenToPremiumTransactions < ActiveRecord::Migration
  def change
    add_column :premium_transactions, :token, :text, after: "status"
  end
end
