class AddVotesCountToUsers < ActiveRecord::Migration

  def self.up

    add_column :users, :votes_count, :integer, :null => false, :default => 0
    
    User.all.each do |user|
      user.update_attributes(votes_count: user.votes.count)
    end

  end

  def self.down

    remove_column :users, :votes_count

  end

end
