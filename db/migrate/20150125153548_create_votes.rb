class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :contestant, index: true
      t.string :ip_address
      t.string :platform
      t.string :browser

      t.timestamps
    end
  end
end
