class CreateContestants < ActiveRecord::Migration
  def change
    create_table :contestants do |t|
      t.references :pageant, index: true
      t.integer :number
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
