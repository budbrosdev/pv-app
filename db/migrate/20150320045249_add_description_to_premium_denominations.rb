class AddDescriptionToPremiumDenominations < ActiveRecord::Migration
  def change
    add_column :premium_denominations, :description, :text, after: "name"
    
    denomination = PremiumDenomination.where(code: "PVP-30D").first_or_create
    denomination.update_attributes(description: "30 Days Premium Membership")
    
    denomination = PremiumDenomination.where(code: "PVP-TRIAL").first_or_create
    denomination.update_attributes(description: "1 Day Trial Membership")
    
    denomination = PremiumDenomination.where(code: "PVP-BV").first_or_create
    denomination.update_attributes(description: "Bulk Votes for a Contestant")
  end
end
