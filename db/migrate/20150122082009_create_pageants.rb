class CreatePageants < ActiveRecord::Migration
  def change
    create_table :pageants do |t|
      t.references :user, index: true
      t.string :name
      t.text :description
      t.datetime :voting_start
      t.datetime :voting_end
      t.boolean :show_votes, default: false
      t.attachment :header
      t.attachment :banner

      t.timestamps
    end
  end
end
