class AddUrlToUsers < ActiveRecord::Migration
  def change
    add_column :users, :url, :string, after: "location"
  end
end
