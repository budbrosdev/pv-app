class GenerateBuyAVoteToContents < ActiveRecord::Migration
  def change
    begin
      new_position = 3
      
      affected_contents = Content.where("position >= ?", new_position).order(:position)
      affected_contents.each_with_index do |content, i|
        content.update_attributes(position: new_position + i + 1)
      end
      
      content = Content.where(area: "buy-a-vote").first_or_create
      content.update_attributes(title: "Buy a Vote", status: 1, position: new_position, description: "Help and support your favourite contestants by buying votes instantly.", script: "<p>Update the instructions from the content editor.</p>")
    rescue; end
  end
end
