class AddThumbToPageants < ActiveRecord::Migration
  def change
    add_attachment :pageants, :thumb, after: "banner_updated_at"
  end
end
