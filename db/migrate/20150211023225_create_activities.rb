class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.references :user, index: true
      t.references :resource, polymorphic: true, index: true

      t.timestamps
    end
    
    Vote.all.each do |vote|
      activity = Activity.create(user: vote.user, resource: vote)
      activity.update_attributes(created_at: vote.created_at, updated_at: vote.updated_at)
    end
    
    PremiumTransaction.all.each do |transaction|
      activity = Activity.create(user: transaction.user, resource: transaction)
      activity.update_attributes(created_at: transaction.created_at, updated_at: transaction.updated_at)
    end
  end
end
