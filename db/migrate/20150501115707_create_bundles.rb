class CreateBundles < ActiveRecord::Migration
  def change
    create_table :bundles do |t|
      t.integer :quantity
      t.decimal :price, precision: 11, scale: 2, default: 0

      t.timestamps
    end
    Bundle.where(price: 10.0, quantity: 10).first_or_create
    Bundle.where(price: 20.0, quantity: 22).first_or_create
    Bundle.where(price: 50.0, quantity: 50).first_or_create
    Bundle.where(price: 100.0, quantity: 120).first_or_create
    Bundle.where(price: 200.0, quantity: 250).first_or_create
  end
end
