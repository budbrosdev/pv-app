class GenerateUserActivities < ActiveRecord::Migration
  def change
    
    User.all.each do |x|
      activity = Activity.create(user: x, resource: x, action: "created")
      activity.update_attributes(created_at: x.created_at, updated_at: x.created_at)
    end
    
  end
end
