class AddTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :token, :text, after: "premium_until"
  end
end
