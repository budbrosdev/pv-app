class AddHomeAdvertisementContent < ActiveRecord::Migration
  def change
    begin
      Content.all.each do |content|
        content.update_attribute(:position, content.position + 1)
      end

      new_content = Content.new
      new_content.area = "home-advertisement"
      new_content.title = "Home Adverisement"
      new_content.script = "<p>Update content from the content editor.</p>"
      new_content.status = :disabled
      new_content.position = 1
      new_content.save
    rescue; end
  end
end
