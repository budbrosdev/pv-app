class AddPageantsCountToUsers < ActiveRecord::Migration

  def self.up

    add_column :users, :pageants_count, :integer, :null => false, :default => 0

    User.all.each do |user|
      user.update_attributes(pageants_count: user.pageants.count)
    end
  end

  def self.down

    remove_column :users, :pageants_count

  end

end
