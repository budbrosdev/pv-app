class FixDataConvertVotesToBought < ActiveRecord::Migration
  def change
    BulkVote.all.each do |bvote|
      if bvote.premium_transaction.present? && bvote.premium_transaction.completed?
        Vote.where(contestant_id: bvote.contestant_id, user_id: bvote.user_id, bought: false).limit(bvote.quantity).update_all(bought: true)
      end
    end
  end
end
