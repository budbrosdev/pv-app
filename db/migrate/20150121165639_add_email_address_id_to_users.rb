class AddEmailAddressIdToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :email_address, index: true, after: "id"
  end
end
