class GenerateUserWallets < ActiveRecord::Migration
  def change
    Pageant.all.pluck(:user_id).uniq.each do |user_id|
      User.find(user_id).prepare_wallet
    end
  end
end
