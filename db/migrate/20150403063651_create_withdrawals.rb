class CreateWithdrawals < ActiveRecord::Migration
  def change
    create_table :withdrawals do |t|
      t.references :user, index: true
      t.string :txn_id
      t.decimal :amount, precision: 11, scale: 2, default: 0.00
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
