class CreateWysiwygImages < ActiveRecord::Migration
  def change
    create_table :wysiwyg_images do |t|
      t.attachment :attachment

      t.timestamps
    end
  end
end
