class AddPaypalEmailToUsers < ActiveRecord::Migration
  def change
    add_column :users, :paypal_email, :string, after: "location"
  end
end
