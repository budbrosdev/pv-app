class FixContents < ActiveRecord::Migration
  def change
    Content.destroy_all
    x = Content.where(area: "home-banner", title: "Home Banner", status: 1).first_or_create
    x.update_attributes(script: "")
    x = Content.where(area: "create-a-pageant", title: "Create a Pageant").first_or_create
    x.update_attributes(script: '<p class="\&quot;fr-tag\&quot; fr-tag" style="text-align: center;" center;\"="">Start creating your first pageant now!</p><p class="\&quot;fr-tag\&quot; fr-tag" style="text-align: center;" center;\"=""><strong><span =""="" style="font-size: 13px;"><span style="color: #F37934; font-size: 22px;"><a href="/signin?next_url=%2Fpageants%2Fnew">Click here to login and create a pageant!</a></span></span></strong></p>')
    Content.where(area: "premium-membership", title: "Premium Membership").first_or_create
    Content.where(area: "how-to-vote", title: "How to Vote").first_or_create
    Content.where(area: "faqs", title: "Frequently Asked Questions").first_or_create
    Content.where(area: "contact-us", title: "Contact Us").first_or_create
  end
end
