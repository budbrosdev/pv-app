class GenerateTermsAndConditionsToContents < ActiveRecord::Migration
  def change
    begin
      new_position = 10
      
      affected_contents = Content.where("position >= ?", new_position).order(:position)
      affected_contents.each_with_index do |content, i|
        content.update_attributes(position: new_position + i + 1)
      end
      
      content = Content.where(area: "terms-and-conditions").first_or_create
      content.update_attributes(title: "Terms & Conditions", status: 1, position: new_position, description: "", script: "<p>Update the content from the content editor.</p>")
    rescue; end
  end
end
