class ChangeShowVotesDefaultValueOfPageants < ActiveRecord::Migration
  def up
    change_column :pageants, :show_votes, :boolean, default: true
  end

  def down
    change_column :pageants, :show_votes, :boolean, default: false
  end
end
