class AddQuantityToBulkVotes < ActiveRecord::Migration
  def change
    add_column :bulk_votes, :quantity, :integer, after: "premium_transaction_id", default: 1
  end
end
