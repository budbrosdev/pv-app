class AddNamesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string, after: "email_address_id"
    add_column :users, :last_name, :string, after: "first_name"
    
    remove_column :users, :name
  end
end
