class FixActivityRecords < ActiveRecord::Migration
  def change
    Pageant.deleted.each do |pageant|
      activity = Activity.create(user: pageant.user, resource: pageant, action: "deleted")
      activity.update_attributes(created_at: pageant.updated_at, updated_at: pageant.updated_at)
    end
    
    Contestant.all.each do |contestant|
      activity = Activity.create(user: contestant.pageant.user, resource: contestant, action: "created")
      activity.update_attributes(created_at: contestant.updated_at, updated_at: contestant.updated_at)
    end
    
  end
end
