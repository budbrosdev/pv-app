class CreatePremiumTransactions < ActiveRecord::Migration
  def change
    create_table :premium_transactions do |t|
      t.references :premium_denomination, index: true
      t.references :user, index: true
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
