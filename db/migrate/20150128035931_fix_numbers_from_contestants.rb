class FixNumbersFromContestants < ActiveRecord::Migration
  def change
    Pageant.all.each do |pageant|
      pageant.contestants.each do |contestant|
        unless contestant.number.present?
          counted_contestants = pageant.contestants.where("contestants.number IS NOT NULL")
          contestant.update_attribute(:number, counted_contestants.count + 1)
        end
      end
    end
  end
end
