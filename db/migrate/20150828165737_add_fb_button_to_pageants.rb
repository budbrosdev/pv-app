class AddFbButtonToPageants < ActiveRecord::Migration
  def change
    add_column :pageants, :fb_button, :text, after: "status"
  end
end
