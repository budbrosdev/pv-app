class FixDataCreditVotesToUsers < ActiveRecord::Migration
  def change
    Vote.all.each do |vote|
      pageant = vote.contestant.pageant
      if vote.bought
        pageant.user.wallet.add_amount(Vote::BLK_PRICE, "", vote)
      else
        pageant.user.wallet.add_amount(Vote::REG_PRICE, "", vote)
      end
    end
  end
end
