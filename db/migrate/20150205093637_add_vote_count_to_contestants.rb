class AddVoteCountToContestants < ActiveRecord::Migration
  def change
    add_column :contestants, :vote_count, :integer, after: "gender", default: nil
  end
end
