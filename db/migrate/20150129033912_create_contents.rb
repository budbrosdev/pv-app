class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :area
      t.string :title
      t.text :script
      t.integer :status, default: 0
      t.integer :position

      t.timestamps
    end

    Content.where(area: "custom-banner", title: "Custom Banner", status: 1).first_or_create
    Content.where(area: "how-to-vote", title: "How to Vote").first_or_create
    Content.where(area: "faqs", title: "Frequently Asked Questions").first_or_create
    Content.where(area: "contact-us", title: "Contact Us").first_or_create

  end
end
