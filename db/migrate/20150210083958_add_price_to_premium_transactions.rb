class AddPriceToPremiumTransactions < ActiveRecord::Migration
  def change
    add_column :premium_transactions, :price, :decimal, precision: 11, scale: 2, default: 0, after: "user_id"
  end
end
