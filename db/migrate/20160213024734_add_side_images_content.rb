class AddSideImagesContent < ActiveRecord::Migration
  def change
    new_content = Content.new
    new_content.area = "side-images"
    new_content.title = "Side Images"
    new_content.script = "<p>Update content from the content editor.</p>"
    new_content.status = :disabled
    new_content.position = Content.maximum(:position) + 1
    new_content.save
  end
end
