class GenerateAdvertisementContent < ActiveRecord::Migration
  def change
    begin
      Content.create(
        area: "advertise-with-us",
        title: "Advertise with us",
        script: "<p>Update content from the content editor.</p>",
        status: 1,
        position: 11,
        description: "Advertise with us"
      )
    rescue; end
  end
end
