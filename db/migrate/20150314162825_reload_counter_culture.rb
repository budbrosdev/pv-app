class ReloadCounterCulture < ActiveRecord::Migration
  def change
    Pageant.counter_culture_fix_counts
    Vote.counter_culture_fix_counts
  end
end
