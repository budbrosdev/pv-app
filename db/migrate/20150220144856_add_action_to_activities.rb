class AddActionToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :action, :string, after: "resource_type"

    Activity.all.each do |activity|
      activity.update_attributes(action: "created") if activity.resource_type == "Pageant"
    end
  end
end