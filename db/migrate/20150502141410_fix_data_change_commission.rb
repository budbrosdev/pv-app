class FixDataChangeCommission < ActiveRecord::Migration
  def change

    puts "Change regular vote commission"
    old_amount = 0.02
    new_amount = 0.05
    Wallet.all.each do |wallet|
      logs = wallet.wallet_logs.where(amount: old_amount)
      missing_credit = logs.count * (new_amount - old_amount)
      if wallet.update_attributes(amount: wallet.amount + missing_credit)
        logs.update_all(amount: new_amount)
      end
    end

    puts "Change buy-a-vote commission"
    old_amount = 4.00
    new_amount = 12.9
    Wallet.all.each do |wallet|
      logs = wallet.wallet_logs.where(amount: old_amount)
      missing_credit = logs.count * (new_amount - old_amount)
      if wallet.update_attributes(amount: wallet.amount + missing_credit)
        logs.update_all(amount: new_amount)
      end
    end

    puts "Recompute wallets"
    Wallet.all.each do |wallet|
      new_amount = wallet.wallet_logs.sum(:amount)
      wallet.update_attributes(amount: new_amount)
    end
  end
end
