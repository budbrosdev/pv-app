class AddQuantityToPremiumTransactions < ActiveRecord::Migration
  def change
    add_column :premium_transactions, :quantity, :integer, after: "price", default: 1
  end
end
