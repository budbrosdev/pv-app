class AddQuantityToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :quantity, :integer, after: "bought", default: 1
  end
end
