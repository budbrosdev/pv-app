class AddFieldsToPremiumTransactions < ActiveRecord::Migration
  def change
    add_column :premium_transactions, :return_params, :text, after: "status"
    add_column :premium_transactions, :txn_id, :string, after: "return_params"
    add_column :premium_transactions, :completed_at, :timestamp, after: "txn_id"
  end
end
