class FixDataRemoveNumberOfDeletedContestants < ActiveRecord::Migration
  def change
    Contestant.deleted.update_all(number: nil)
  end
end
