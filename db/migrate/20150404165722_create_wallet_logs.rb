class CreateWalletLogs < ActiveRecord::Migration
  def change
    create_table :wallet_logs do |t|
      t.references :wallet, index: true
      t.references :resource, polymorphic: true, index: true
      t.decimal :amount, precision: 11, scale: 2, default: 0.00
      t.text :note

      t.timestamps
    end
  end
end
