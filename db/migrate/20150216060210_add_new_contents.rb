class AddNewContents < ActiveRecord::Migration
  def change
    content = Content.find_by(area: "faqs")
    content.update_attributes(area: "faq", title: "Home Banner") if content.present?

    content = Content.find_by(area: "home-banner")
    content.update_attributes(position: 1) if content.present?

    content = Content.find_by(area: "create-a-pageant")
    content.update_attributes(position: 2) if content.present?

    content = Content.find_by(area: "premium-membership")
    content.update_attributes(position: 3) if content.present?

    content = Content.find_by(area: "how-to-vote")
    content.update_attributes(position: 4) if content.present?
    
    content = Content.create(position: 5, area: "what-is-autolike", title: "What is an Autolike?")

    content = Content.find_by(area: "faq")
    content.update_attributes(position: 6) if content.present?

    content = Content.find_by(area: "contact-us")
    content.update_attributes(position: 7) if content.present?
    
  end
end
