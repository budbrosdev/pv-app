class AddVotesCountToContestants < ActiveRecord::Migration

  def self.up

    add_column :contestants, :votes_count, :integer, :null => false, :default => 0
    
    Contestant.all.each do |contestant|
      contestant.update_attributes(votes_count: contestant.votes.count)
    end
  end

  def self.down

    remove_column :contestants, :votes_count

  end

end
