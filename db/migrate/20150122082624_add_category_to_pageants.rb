class AddCategoryToPageants < ActiveRecord::Migration
  def change
    add_column :pageants, :category, :integer, after: "description", default: 0
  end
end
