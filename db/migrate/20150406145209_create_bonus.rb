class CreateBonus < ActiveRecord::Migration
  def change
    create_table :bonus do |t|
      t.references :user, index: true
      t.decimal :amount, precision: 11, scale: 2, default: 0.00
      t.text :notes

      t.timestamps
    end
  end
end
