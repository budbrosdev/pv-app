class GeneratePageantActivities < ActiveRecord::Migration
  def change
    Pageant.all.each do |pageant|
      activity = Activity.create(user: pageant.user, resource: pageant, action: "created")
      activity.update_attributes(created_at: pageant.created_at, updated_at: pageant.updated_at)
    end
  end
end
