class GenerateBuyAVoteDenomination < ActiveRecord::Migration
  def change
    PremiumDenomination.create(code: "PVP-BV", name: "Pageant Vote (Buy a Vote)", price: 0.99, validity: 0)
  end
end
