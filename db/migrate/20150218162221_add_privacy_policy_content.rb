class AddPrivacyPolicyContent < ActiveRecord::Migration
  def change
    
    content = Content.where(area: "privacy-policy").first_or_create
    content.update_attributes(title: "Privacy Policy", status: 0, position: Content.count)
    
  end
end
