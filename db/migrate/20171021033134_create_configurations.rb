class CreateConfigurations < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :title
      t.string :key
      t.text :value, limit: 16777215

      t.timestamps
    end
  end
end
