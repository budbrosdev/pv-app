class AddTxnIdToPremiumTransactions < ActiveRecord::Migration
  def change
    add_column :premium_transactions, :txn_id, :string, after: "payer_id"
  end
end
