class AddBoughtToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :bought, :boolean, default: false, after: "user_id"
  end
end
