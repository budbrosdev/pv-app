class AddBoostVotesToPageants < ActiveRecord::Migration
  def change
    add_column :pageants, :boost_votes, :boolean, after: "show_votes", default: false
  end
end
