class AddSideScriptContent < ActiveRecord::Migration
  def change
    new_content = Content.new
    new_content.area = "side-script"
    new_content.title = "Side Script"
    new_content.script = "Update content from the content editor."
    new_content.status = :disabled
    new_content.position = Content.maximum(:position) + 1
    new_content.save
  end
end
