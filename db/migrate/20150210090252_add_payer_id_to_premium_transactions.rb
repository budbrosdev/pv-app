class AddPayerIdToPremiumTransactions < ActiveRecord::Migration
  def change
    add_column :premium_transactions, :payer_id, :string, after: "token"
    remove_column :premium_transactions, :return_params
    remove_column :premium_transactions, :txn_id
  end
end
