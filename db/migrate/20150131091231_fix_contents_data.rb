class FixContentsData < ActiveRecord::Migration
  def change
    content = Content.find_by(area: "custom-banner")
    content.update_attributes(area: "home-banner", title: "Home Banner") if content.present?
  end
end
