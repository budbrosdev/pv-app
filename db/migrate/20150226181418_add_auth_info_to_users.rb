class AddAuthInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :auth, :text, after: "premium_until"
  end
end
