class FixDataRemoveUsersWithoutEmail < ActiveRecord::Migration
  def change
    User.where(email: nil).destroy_all
  end
end
