class AddStatusToPageants < ActiveRecord::Migration
  def change
    add_column :pageants, :status, :integer, after: "show_votes", default: 0
  end
end
