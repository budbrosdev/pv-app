class AddInfoToEmailAddresses < ActiveRecord::Migration
  def change
    add_column :email_addresses, :first_name, :string, after: "email"
    add_column :email_addresses, :last_name, :string, after: "first_name"
    add_column :email_addresses, :gender, :integer, after: "last_name", default: 0
    add_column :email_addresses, :birth_date, :date, after: "gender"
    add_column :email_addresses, :location, :string, after: "birth_date"
    add_column :email_addresses, :role, :string, after: "location"
    
    EmailAddress.all.each do |e|
      user = e.users.first
      e.update_attributes(first_name: user.first_name, last_name: user.last_name, role: user.role)
    end
    
    
    remove_column :users, :first_name
    remove_column :users, :last_name
    remove_column :users, :role
    rename_column :users, :email_address_id, :user_id
    
    rename_table :users, :authentications
    
    rename_table :email_addresses, :users
    
  end
end
