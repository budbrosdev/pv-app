class AddShareToBundles < ActiveRecord::Migration
  def change
    add_column :bundles, :share, :decimal, precision: 11, scale: 2, default: 0, after: "price"

    Bundle.where(price: 10.0).first.update_attributes(share: 0)
    Bundle.where(price: 20.0).first.update_attributes(share: 6)
    Bundle.where(price: 50.0).first.update_attributes(share: 15)
    Bundle.where(price: 100.0).first.update_attributes(share: 30)
    Bundle.where(price: 200.0).first.update_attributes(share: 60)
  end
end
