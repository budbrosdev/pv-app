class AddGenderToContestants < ActiveRecord::Migration
  def change
    add_column :contestants, :gender, :integer, after: "description", default: 0
  end
end
