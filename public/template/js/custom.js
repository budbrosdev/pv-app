/* #Bootstrap
================================================== */

!function ($) {

  $(function(){

    var $window = $(window)

    // Disable certain links in docs
    $('section [href^=#]').click(function (e) {
      e.preventDefault()
    })
        
    $('#myTab a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    })

    // make code pretty
    window.prettyPrint && prettyPrint()

    // add-ons
    $('.add-on :checkbox').on('click', function () {
      var $this = $(this)
        , method = $this.attr('checked') ? 'addClass' : 'removeClass'
      $(this).parents('.add-on')[method]('active')
    })

    // tooltip 
    $('.tooltip').tooltip({
      selector: "a[rel=tooltip]"
    })
    $("[rel=tooltip]").tooltip();
        
    // alert boxes
    $(".alert").alert()    
    
    // popover
    $('.popover').popover()

    // popover 
    $("a[rel=popover]")
      .popover()
      .click(function(e) {
        e.preventDefault()
      })

    // carousel         
    $('#myCarousel').carousel({
        pause: "false"
    });

    // parallax         
    $('#parallax').carousel();
})

}(window.jQuery)


/* #DROP-DOWN NAVIGATION
================================================== */
$("<select />").appendTo(".navigation");

// Create default option "Go to..."
$("<option />", {
   "selected": "selected",
   "value"   : "",
   "text"    : "Go to..."
}).appendTo(".navigation select");

// Populate dropdown with menu items
$(".navigation a").each(function() {
 var el = $(this);
 $("<option />", {
     "value"   : el.attr("href"),
     "text"    : el.text()
 }).appendTo(".navigation select");
});
$(".navigation select").change(function() {
  window.location = $(this).find("option:selected").val();
});

(function ($) {
  // hash change handler
  function hashchange () {
    var hash = window.location.hash
      , el = $('ul.tabs [href*="' + hash + '"]')
      , content = $(hash)

    if (el.length && !el.hasClass('active') && content.length) {
      el.closest('.tabs').find('.active').removeClass('active');
      el.addClass('active');
      content.show().addClass('active').siblings().hide().removeClass('active');
    }
  }

  // listen on event and fire right away
  $(window).on('hashchange.skeleton', hashchange);
  hashchange();
  $(hashchange);
})(jQuery);

jQuery(document).ready(function ($) {
	
/* FlexSlider
================================================== */

(function(){
    
    if($('.flexslider').length) {
        
        $(window).load(function(){
            
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: true,
                animationSpeed: 300,
                itemWidth: 245,
                asNavFor: '#slider'
            });
  
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                directionNav: false,
                animationLoop: true,
                slideshow: true,
                sync: "#carousel"
                
            });
            
        });	
        
    }

})();




});


