require 'test_helper'

class PageantsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get edit" do
    get :edit
    assert_response :success
  end

  test "should get current" do
    get :current
    assert_response :success
  end

  test "should get past" do
    get :past
    assert_response :success
  end

end
