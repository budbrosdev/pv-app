set :application, 'pv'
set :repo_url, 'git@bitbucket.org:budbrosdev/pv-app.git'
set :deploy_via, :remote_cache
set :keep_releases, 3
set :linked_files, %w{config/database.yml config/application.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :rake, "bundle exec rake"
set :stage, lambda{ config_name.split(':').last }

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :finishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end

