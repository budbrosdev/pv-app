Rails.application.routes.draw do

  root to: 'visitors#home'

  get '/error' => 'visitors#error', as: 'error'
  get '/whatsmyip' => 'visitors#ip_address', as: 'ip_address'
  get '/visitor/signin' => 'visitors#signin', as: :signin_page
  get '/auth/:provider/callback' => 'sessions#create'
  get '/signin' => 'sessions#new', :as => :signin
  get '/signout' => 'sessions#destroy', :as => :signout
  get '/error' => 'visitors#error'
  get '/home' => 'visitors#index', as: "home"
  get '/auth/failure' => 'sessions#failure'

  get "/paypal_success" => "users#paypal_success", as: "paypal_success"
  get "/paypal_failed" => "users#paypal_failed", as: "paypal_failed"

  post '/users/me' => 'users#show'

  get "/current" => "pageants#current", as: "current_pageants"
  get "/past" => "pageants#past", as: "past_pageants"
  get "/all" => "pageants#all", as: "all_pageants"
  get "/pageants/current" => redirect("/current")
  get "/pageants/past" => redirect("/past")
  get "/pageants/all" => redirect("/all")

  resources :users, except: [:new, :delete] do
    collection do
      post :go_premium
      post :buy_votes
    end
    member do
      get :account
      get :register_premium
      post :block
      post :unblock
      post :withdraw
      post :give_bonus
    end
  end

  resources :withdrawals, only: [:index] do
    collection do
      post :complete
      post :reject
    end
    member do
      post :approve
    end
  end

  resources :settings, only: [] do
    collection do
      get :edit
      post :update
    end
  end

  resources :transactions, only: [:create]

  get '/info/:id' => 'contents#show', as: :info
  resources :contents, only: [:index, :show, :edit, :update] do
    collection do
      post :upload_image
    end
  end

  resources :pageants do
    member do
      post :publish
      post :unpublish
    end
    resources :contestants, except: [:index] do
      member do
        post :boost_vote
        get :vote
        get :votes
        get :upload
        post :upload_process
        post :delete_photo
      end
    end
  end
end
