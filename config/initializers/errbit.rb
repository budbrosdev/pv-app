Airbrake.configure do |config|
  config.host = 'http://errors.ikennonline.com'
  config.project_id = 1 # required, but any positive integer works
  config.project_key = 'c07bffa6e67bba4fa672bf3322e5ba00'

  # Uncomment for Rails apps
  config.environment = Rails.env
  config.ignore_environments = %w(development test)
end