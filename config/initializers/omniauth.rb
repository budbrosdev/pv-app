Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV["OMNIAUTH_PROVIDER_KEY"], ENV["OMNIAUTH_PROVIDER_SECRET"],
            scope: "email,public_profile,user_location",
            info_fields: "id,email,gender,first_name,last_name,location,locale,name,timezone,updated_time,verified,birthday"
end
