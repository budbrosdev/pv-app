module ApplicationHelper
  
  def format_datetime(date)
    return nil unless date.present?
    date.strftime("%b %d, %Y %l:%M%p")
  end
  
  def format_date(date)
    return nil unless date.present?
    (date.in_time_zone(ENV['TIMEZONE'])).strftime("%B %d, %Y")
  end
  
  def format_duration(date1, date2)
    if date1.year == date2.year
      if date1.month == date2.month
        "#{date1.strftime('%B %e')} - #{date2.strftime('%e')}, #{date1.strftime('%Y')}"
      else
        "#{date1.strftime('%B %e')} - #{date2.strftime('%B %e')}, #{date1.strftime('%Y')}"
      end
    else
      "#{date1.strftime('%B %e, %Y')} - #{date2.strftime('%B %e, %Y')}"
    end
  end
  
end
