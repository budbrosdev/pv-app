$ ->
  $(document).ready boostVotesHooks
  $(document).on "page:load", boostVotesHooks

boostVotesHooks = ->
  if $.urlParam("go") == "boost_votes"
    $(".boost-votes-details").toggle()
    $(".info-details").toggle()

  $(document).on "click", ".boost-votes-button, .cancel-boost-votes-button", (e)->
    e.preventDefault
    $(".boost-votes-details").toggle()
    $(".info-details").toggle()

$.urlParam = (name) ->
  results = new RegExp('[?&]' + name + '=([^]*)').exec(window.location.href)
  if results == null
    null
  else
    results[1].split("#")[0] or 0