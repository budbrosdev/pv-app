$ ->
  $.contestant_gallery_slideshow()
  $(document).ready customHooks
  $(document).on "page:load", customHooks

customHooks = ->
  $.contestant_gallery_slideshow()
  $(window).load ->
    $(".slidesjs-container, .slidesjs-control").css("height", $(".main-image:visible").height())
  $(window).resize ->
    $(".slidesjs-container, .slidesjs-control").css("height", $(".main-image:visible").height())
    return

  $("#contestant_upload_modal_button").on "click", (e) ->
    e.preventDefault()
    parent = $("#contestant_upload")
    form = parent.find("form")
    parent.find(".alert").remove()
    #parent.find("button[data-form-target=#{form.prop('id')}]").removeAttr("disabled")

  $("#contestant_upload :file").on "change", () ->
    parent = $("#contestant_upload")
    form = parent.find("form")
    selected_files = parseInt($(this).prop("files").length)
    uploaded = parseInt(form.data("uploaded-count"))
    limit = parseInt(form.data("photo-limit"))
    total = selected_files + uploaded
    submit_button = parent.find(".btn[data-form-target=#{form.prop('id')}]")
    if total > limit
      submit_button.prop("disabled", true)
      $.inline_alert_message(form.find(".form-error-message"), "You can only have up to #{limit} photos per contestant.")
    else
      parent.find(".alert").remove()
      submit_button.removeAttr("disabled")


$.contestant_gallery_slideshow = () ->
  if $(".main-image-slider .main-image").length > 1
    $(".main-image-slider").unbind()
    $(".main-image-slider").slidesjs
      width: 300
      height: 410
      navigation: active: false
      play:
        active: false
        auto: true
        interval: 5000
        swap: true
      callback: loaded: (number) ->
        $('.slidesjs-pagination-item').each (index, element) ->
          target = $(element).find('a')
          main_image = $('.main-image-slider [slidesjs-index=' + index + ']')
          src = main_image.find(".gallery-image").data('image-thumb')
          $(target).html "<div class='image'><img src='#{src}' alt='' /></div>"
          if main_image.hasClass("admin")
            $(element).addClass("admin")
            $(this).prepend main_image.find(".action").clone()
            $(this).find("a.delete_photo").on "click", (e) ->
              e.preventDefault()
              $.custom_confirm_modal($(this))