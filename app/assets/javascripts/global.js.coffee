$ ->
  $("<link/>",
      rel: "stylesheet"
      type: "text/css"
      href: "//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css"
    ).appendTo "head"

  $(document).ready globalHooks
  $(document).on "page:load", globalHooks
  $(document).ajaxStop (e)->
    $.fn.reloadHooks()

  $.countdown()
  setInterval (->
    $.countdown()
    return
  ), 1000

content_buttons = ['resetField', 'sep', 'undo', 'redo', 'sep', 'bold', 'italic', 'underline', 'sep', 'fontFamily', 'fontSize', 'color', 'removeFormat', 'sep', 'align', 'indent', 'outdent', 'sep', 'createLink', 'table', 'insertOrderedList', 'insertUnorderedList', 'insertHorizontalRule', 'sep', 'insertImage', 'insertVideo', 'sep', 'html']

$.fn.reloadHooks = ->
  if $("[tip]").size() > 0
    $('[tip]').each ->
      $(this).tooltip
        title: $(this).attr("tip")
        placement: "auto"
        animation: false
        container: 'body'

globalHooks = ->

  $.fn.reloadHooks()

  if Cookies.get("skipped_fb_page") == undefined
    Cookies.set("skipped_fb_page", "0")

  if $("#fb-plugin-modal.modal").size() == 1
    pageant_id = $("#fb-plugin-modal.modal").data("pageant-id")
    skipped = Cookies.get("skipped_fb_page").split(",")
    if $.inArray("#{pageant_id}", skipped) == -1
      $("#fb-plugin-modal.modal").modal()

  $(".fb-page-liked").on "click", (e)->
    e.preventDefault()
    pageant_id = $(this).parents("#fb-plugin-modal").data("pageant-id")
    skipped = Cookies.get("skipped_fb_page").split(",")
    if $.inArray("#{pageant_id}", skipped) == -1
      Cookies.set("skipped_fb_page", "#{skipped},#{pageant_id}")
    $("#fb-plugin-modal.modal").modal('hide')

  $(".fb-page-thanks").on "click", (e)->
    e.preventDefault()
    $("#fb-plugin-modal.modal").modal('hide')

  $(".no-link").on "click", (e)->
    e.preventDefault()

  $("a[data-confirm]").on "click", (e) ->
    e.preventDefault()
    $.custom_confirm_modal($(this))

  $("[data-modal-target]").on "click", (e) ->
    e.preventDefault()
    id = $(this).data("modal-target")
    if id.search('#') < 0
      element = $("##{id}")
    else
      element = $(id)
    element.modal()
    $.clear_form_fields(element)

  $("[data-form-target]").on "click", (e) ->
    e.preventDefault()
    id = $(this).data("form-target")
    if id.search('#') < 0
      element = $("##{id}")
    else
      element = $(id)
    element.submit()

  $('.wysiwyg').editable
    theme: 'gray'
    inlineMode: false
    tabSpaces: true
    placeholder: ''
    minHeight: 10
    imageUploadParam: "image"
    imageUploadURL: "/contents/upload_image"
    fontList: ["Arial, Helvetica", "Impact, Charcoal", "Tahoma, Geneva"]
    buttons: content_buttons

  $('.date-basic').datetimepicker
    format: "Y-m-d"
    yearStart: new Date().getFullYear() - 60
    yearEnd: new Date().getFullYear() - 15
    step: 30
    timepicker: false

  $('.datetime-basic').datetimepicker
    format: "Y-m-d g:iA"
    formatTime: "g:iA"
    yearStart: new Date().getFullYear() - 5
    yearEnd: new Date().getFullYear() + 5
    step: 30
    timepickerScrollbar: false

  $(document).on "keypress", "input.alpha-numeric", (e) ->
    false unless /[0-9a-zA-Z-]/.test(String.fromCharCode(e.which))

  $(document).on "keypress", "input.hex", (e) ->
    false unless /[0-9a-fA-F]/.test(String.fromCharCode(e.which))

  $(document).on "keypress", "input.numeric", (e) ->
    if $(this).data("num-negative")
      return false unless /[0-9]|\-/.test(String.fromCharCode(e.which))
    else
      return false unless /[0-9]/.test(String.fromCharCode(e.which))

  $(document).on "click", ".complete_withdrawal_button", (e)->
    $("#complete_withdrawal.modal").modal("show")
    $("form#complete_withdrawal_form input#withdrawal_id").val($(this).data("id"))
    $("form#complete_withdrawal_form input#withdrawal_user_id").val($(this).data("email"))
    $("form#complete_withdrawal_form input#withdrawal_transaction_id").val("").focus()

  $(document).on "click", ".reject_withdrawal_button", (e)->
    $("#reject_withdrawal.modal").modal("show")
    $("form#reject_withdrawal_form input#withdrawal_id").val($(this).data("id"))
    $("form#reject_withdrawal_form input#withdrawal_user_id").val($(this).data("email"))
    $("form#reject_withdrawal_form input#withdrawal_notes").val("").focus()

  $(document).on "change", "input.numeric", (e) ->
    if $(this).hasClass("numeric-validation")
      val = parseInt($(this).val())
      min = parseInt($(this).data("num-min") || "0")
      max = parseInt($(this).data("num-max") || "0")
      if $(this).val() == ""
        console.log $(this).data("num-default") == undefined
        if $(this).data("num-default") == undefined
          $(this).val(min)
        else
          $(this).val(parseInt($(this).data("num-default")))
      else if val < min
        $(this).val(min)
      else if val > max
        $(this).val(max)

$.custom_confirm_modal = (element) ->
  message = element.data('confirm')
  subtitle = element.data('confirm-subtitle')
  logout_msg = element.data('logout-msg')
  return true unless message
  $link = element.clone()
    .removeAttr('class')
    .removeAttr('data-confirm')
    .addClass('btn btn-default')
    .attr("data-icon", "")
    .html("OK")
  $link.click ->
    $(".modal.fade").modal('hide')
    if element.data("target") != null
      form = $("form#"+element.data("target"))
      if form.length > 0
        form.submit()

  modal_html = "<div id='custom_confirm_modal' class='modal fade'>"
  modal_html += " <div class='modal-dialog'>"
  modal_html += "   <div class='modal-content'>"
  modal_html += "     <div class='modal-header'>"
  modal_html += "       <h4><i class='fa fa-exclamation-circle'></i> Important</h4>"
  modal_html += "     </div>"
  modal_html += "     <div class='modal-body acenter'>"
  modal_html += "       <h3>#{message}</h3>"
  if subtitle != undefined
    modal_html += "       <small>#{subtitle}</small>"
  if logout_msg != undefined
    modal_html += "       <br><a class='btn' href='/signout' id='logout_link'>#{logout_msg}</a>"
  modal_html += "     </div>"
  modal_html += "     <div class='modal-footer'>"
  modal_html += "       <div class='btn' data-dismiss='modal'>Cancel</div>"
  modal_html += "     </div>"
  modal_html += "   </div>"
  modal_html += " </div>"
  modal_html += "</div>"

  $modal_html = $(modal_html)
  $modal_html.find('.modal-footer').append($link)
  $modal_html.modal
    backdrop: 'static'
  return false

$.inline_alert_message = (parent, msg, default_klass) ->
  default_klass = default_klass || "alert-danger"
  content = "<div class='alert #{default_klass}' role='alert'>"
  content += "<button type='button' class='close' data-dismiss='alert'>&times;</button>"
  content += msg
  content += "</div>"
  parent.html(content)

$.clear_form_fields = (parent) ->
  $(parent).find(":input").each ->
    switch @type
      when "password", "text", "textarea", "file", "select-one", "select-multiple"
        $(this).val ""
      when "checkbox", "radio"
        @checked = false
    return
  return

$.countdown = ->
  delta = $(".counter_table").data("remaining-seconds")
  $(".counter_table").data "remaining-seconds", delta  - 1

  # calculate (and subtract) whole days
  days = Math.floor(delta / 86400)
  delta -= days * 86400

  # calculate (and subtract) whole hours
  hours = Math.floor(delta / 3600) % 24
  delta -= hours * 3600

  # calculate (and subtract) whole minutes
  minutes = Math.floor(delta / 60) % 60
  delta -= minutes * 60

  # what's left is seconds
  seconds = delta % 60

  $(".days").html days
  $(".hours").html hours
  $(".minutes").html minutes
  $(".seconds").html seconds
