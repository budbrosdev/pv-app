$ ->
  $(document).ready customHooks
  $(document).on "page:load", customHooks
  
customHooks = ->

# #Bootstrap
#================================================== 
  $window = $(window)
  
  # Disable certain links in docs
  $("section [href^=#]").click (e) ->
    e.preventDefault()
    return

  $("#myTab a").click (e) ->
    e.preventDefault()
    $(this).tab "show"
    return

  
  # make code pretty
  window.prettyPrint and prettyPrint()
  
  # add-ons
  $(".add-on :checkbox").on "click", ->
    $this = $(this)
    method = (if $this.attr("checked") then "addClass" else "removeClass")
    $(this).parents(".add-on")[method] "active"
    return

  
  # tooltip 
  $(".tooltip").tooltip selector: "a[rel=tooltip]"
  $("[rel=tooltip]").tooltip()
  
  # alert boxes
  $(".alert").alert()
  
  # popover
  $(".popover").popover()
  
  # popover 
  $("a[rel=popover]").popover().click (e) ->
    e.preventDefault()
    return

  
  # carousel         
  $("#myCarousel").carousel pause: "false"
  
  # parallax         
  $("#parallax").carousel()
  return

  # #DROP-DOWN NAVIGATION
  #================================================== 
  $("<select />").appendTo ".navigation"

  # Create default option "Go to..."
  $("<option />",
    selected: "selected"
    value: ""
    text: "Go to..."
  ).appendTo ".navigation select"

  # Populate dropdown with menu items
  $(".navigation a").each ->
    el = $(this)
    $("<option />",
      value: el.attr("href")
      text: el.text()
    ).appendTo ".navigation select"
    return

  $(".navigation select").change ->
    window.location = $(this).find("option:selected").val()
  
    # hash change handler
hashchange = ->
  hash = window.location.hash
  el = $("ul.tabs [href*=\"" + hash + "\"]")
  content = $(hash)
  if el.length and not el.hasClass("active") and content.length
    el.closest(".tabs").find(".active").removeClass "active"
    el.addClass "active"
    content.show().addClass("active").siblings().hide().removeClass "active"
  return