$ ->
  $(document).ready contentHooks
  $(document).on "page:load", contentHooks

contentHooks = ->
  $(document).on "click", "#new_bulk_vote .button", (e)->
    e.preventDefault()
    $(".pageant-contestants:not(:visible)").remove()
    $("form#new_bulk_vote").submit()
  
  $(document).on "change", "#bulk_vote_pageant_id", (e) ->
    selected_id = $(this).val()
    $(".pageant-contestants:visible").hide()
    $(".vote-quantity").hide()
    $(".form-buttons").hide()
    $(".pageant-contestants[data-id=#{selected_id}] select").val("")
    $(".pageant-contestants[data-id=#{selected_id}]").show()
    
  $(document).on "change", ".bulk-vote-contestant", (e) ->
    $(".vote-quantity input").val("1")
    $(".form-buttons").show()
    $(".vote-quantity").show()