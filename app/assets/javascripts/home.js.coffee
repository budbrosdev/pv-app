$ ->
  $(document).ready customHooks
  $(document).on "page:load", customHooks

customHooks = ->
  if $(".home-slider-container").length > 0
    $.run_home_slider($(".home-slider-container"))

$.run_home_slider = (parent) ->
  parent.find("img:gt(0)").hide()
  setInterval (->
    parent.find("img:first-child").fadeOut()
    .next("img").fadeIn()
    .end().appendTo(parent)
    return
  ), 5000