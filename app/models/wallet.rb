class Wallet < ActiveRecord::Base
  belongs_to :user
  has_many :wallet_logs, dependent: :destroy

  def add_amount(value, note = nil, object = nil)
    return false unless Withdrawal.valid_amount?(value)
    log = self.wallet_logs.create(amount: value, note: note, resource: object)
    return self.update_attributes(amount: self.amount + log.amount)
  end

  def add_bonus(value, notes = nil)
    return false unless Withdrawal.valid_amount?(value)
    bonus = Bonus.create(user_id: user_id, amount: value, notes: notes)
    log = self.wallet_logs.create(amount: value, note: notes, resource: bonus)
    return self.update_attributes(amount: self.amount + log.amount)
  end

  def self.sanitize(clean = false)
    log "Fix wallet logs"
    log ""

    total_profit = 0.0
    total_bonus = 0.0
    total_withdrawal = 0.0
    total_wallet_before = 0.0
    total_wallet_after = 0.0
    total_adjustment = 0.0

    Wallet.all.each do |wallet|
      user = wallet.user
      credits_before = wallet.amount
      log "User##{user.id} -- #{user.name} -- #{'%.2f' % wallet.amount}"

      profit = user.total_profit
      bonus = user.bonus.sum(:amount)
      withdrawal = user.total_withdrawal

      log "---- Pageant Profit: #{'%.2f' % profit}"
      log "---- Bonus Received: #{'%.2f' % bonus}"
      log "---- Successfull Withdrawal: #{'%.2f' % withdrawal}"

      credits_after = (profit + bonus) - withdrawal

      if credits_before == credits_after
        diff = 0.0
      else
        diff = credits_before - credits_after
      end

      if diff != 0
        total_adjustment += diff
      end
      total_wallet_before += credits_before
      total_wallet_after += credits_after
      total_profit += profit
      total_bonus += bonus
      total_withdrawal += withdrawal

      log "---- Difference: #{'%.2f' % diff}"

      if clean
        if diff.to_f != 0
          wallet.update(amount: credits_after)
          log "---- The wallet is updated from #{'%.2f' % credits_before} to #{'%.2f' % credits_after}"
        else
          log "---- The wallet is not changed -- #{'%.2f' % wallet.amount}"
        end
      else
        if diff.to_f != 0
          log "---- The wallet will be changed from #{'%.2f' % credits_before} to #{'%.2f' % credits_after}"
        else
          log "---- The wallet will not be changed -- #{'%.2f' % wallet.amount}"
        end
      end
      log ""
    end
    log ""
    log "-- Total Profit: #{'%.2f' % total_profit}"
    log "-- Total Bonus: #{'%.2f' % total_bonus}"
    log "-- Total Withdrawal: #{'%.2f' % total_withdrawal}"
    log ""
    log "-- Total Wallet Before: #{'%.2f' % total_wallet_before}"
    log "-- Total Wallet After: #{'%.2f' % total_wallet_after}"
    log "-- Total Adjustments: #{'%.2f' % total_adjustment}"
    return nil
  end

  private

  def self.log(msg)
    return puts msg if Rails.env.development?
    Rails.logger.info msg
  end
end
