class BulkVote < ActiveRecord::Base
  belongs_to :user
  belongs_to :pageant
  belongs_to :contestant
  belongs_to :premium_transaction

  def commission
    premium_transaction.price.to_f * 0.7
  end
end
