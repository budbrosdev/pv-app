class WalletLog < ActiveRecord::Base
  belongs_to :wallet
  belongs_to :resource, polymorphic: true
end
