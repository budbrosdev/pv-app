class Pageant < ActiveRecord::Base
  paginates_per 20

  enum status: [:draft, :published, :unpublished, :deleted]
  enum category: [:male_and_female, :male_only, :female_only]

  after_create :record_activity

  has_attached_file :header, :styles => {cropped: "1000x300#"}
  has_attached_file :banner, :styles => {cropped: "690x315#"}
  has_attached_file :thumb, :styles => {cropped: "470x245#"}, default_url: "http://#{ENV['DOMAIN_NAME']}/assets/#{ENV['OG_IMAGE']}"
  validates_attachment_content_type :header, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :banner, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :thumb, :content_type => /\Aimage\/.*\Z/

  belongs_to :user
  counter_culture :user
  has_many :contestants, dependent: :destroy
  has_many :bulk_votes

  validates :name, :voting_start, :voting_end, :category, presence: true
  validate :voting_end_should_be_greater_than_voting_start

  scope :not_deleted, -> {where.not(status: Pageant.statuses[:deleted])}
  scope :current, -> {where("voting_end > ?", Time.now.utc)}
  scope :past, -> {where("voting_end <= ?", Time.now.utc)}
  scope :exclude_ids, ->(ids){where("id NOT IN (?)", ids)}
  scope :sort_start, ->{order("pageants.voting_start DESC")}

  def available_votes_of_user(user, gender)
    contestant_ids = male_and_female? ? contestants.send(gender).pluck(:id) : contestants.pluck(:id)
    date = Time.now.in_time_zone(ENV['TIMEZONE']).strftime("%Y-%m-%d")
    offset = Time.now.strftime("%z").gsub("0","").gsub("-", "- INTERVAL ").gsub("+", "+ INTERVAL ")
    vote_spent = user.votes.single.where("DATE(created_at #{offset} HOUR) = DATE(?) AND contestant_id IN (?)", date, contestant_ids).count
    user.vote_per_day - vote_spent
  end

  def profit
    regular_votes = votes.where(bought: false).count(:id)
    bulk_votes = votes.where(bought: true).count(:id)
    (regular_votes * Vote::REG_PRICE) + (bulk_votes * Vote::BLK_PRICE)
  end

  def votes
    Vote.where(contestant_id: contestants.pluck(:id))
  end

  def completed_bulk_votes
    bulk_votes.joins(:premium_transaction).where("premium_transactions.status = 1")
  end

  def voting_end_should_be_greater_than_voting_start
    if voting_start.present? && voting_end.present?
      if voting_start > voting_end
        errors.add(:voting_end, "should be greater than or equal to start date")
      end
    end
  end

  def authorized?(current_user = nil)
    return false unless current_user.present?
    current_user.admin? || current_user.vip? || current_user == self.user
  end

  def other(limit = 5)
    Pageant.published.not_deleted.where("voting_end > ? AND id <> ?", Time.now.utc, id).order("RAND()").limit(4)
  end

  def top(limit = 5, gender = nil)
    if gender.nil?
      results = contestants.not_deleted.joins(:votes).select("contestants.*, COUNT(votes.id) as vote_count").group("contestants.id").order("vote_count DESC").limit(limit)
      results = contestants.not_deleted.where("contestants.vote_count IS NOT NULL").order("contestants.vote_count DESC").limit(limit) unless results.present?
    elsif gender == "male"
      results = contestants.not_deleted.male.joins(:votes).select("contestants.*, COUNT(votes.id) as vote_count").group("contestants.id").order("vote_count DESC").limit(limit)
      results = contestants.not_deleted.male.where("contestants.vote_count IS NOT NULL").order("contestants.vote_count DESC").limit(limit) unless results.present?
    else
      results = contestants.not_deleted.female.joins(:votes).select("contestants.*, COUNT(votes.id) as vote_count").group("contestants.id").order("vote_count DESC").limit(limit)
      results = contestants.not_deleted.female.where("contestants.vote_count IS NOT NULL").order("contestants.vote_count DESC").limit(limit) unless results.present?
    end
    return results
  end

  def published_status
    if voting_start > Time.now
      "upcoming"
    else
      if voting_end > Time.now
        "ongoing"
      else
        "done"
      end
    end
  end

  def upcoming?
    voting_start > Time.now
  end

  def ongoing?
    voting_start < Time.now && voting_end > Time.now
  end

  def done?
    voting_start < Time.now && voting_end < Time.now
  end

  def default_gender
    if male_only?
      Contestant.genders["male"]
    elsif female_only?
      Contestant.genders["female"]
    end
  end

  def record_activity
    Activity.create(user: user, resource: self, created_at: created_at, updated_at: updated_at, action: 'created')
  end

  def self.log(msg)
    return puts msg if Rails.env.development?
    Rails.logger.info msg
  end

  def self.sanitize_votes(pagent_ids, delete = false)
    pagent_ids.each do |id|
      pageant = Pageant.find_by(id: id)
      next unless pageant.present?

      log ""
      log "Fix votes for Pageant##{pageant.id} - #{pageant.name}"

      mark_date = "2017-03-25".to_date
      date = pageant.voting_start.to_date
      date = mark_date if date < mark_date
      total_votes = 0
      total_votes_to_delete = 0
      total_votes_deleted = 0
      while date <= pageant.voting_end.to_date && date <= Date.today do
        offset = Time.now.strftime("%z").gsub("0","").gsub("-", "- INTERVAL ").gsub("+", "+ INTERVAL ")
        vote_count = Vote.where("contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", pageant.contestants.pluck(:id), date).count
        total_votes += vote_count
        if vote_count > 0
          log ""
          log "Sanitize Votes for #{date} - #{vote_count} vote#{'s' if vote_count > 1}"
          to_delete, deleted = Vote.sanitize_pageant(pageant.id, date, delete)
          if to_delete > 1
            log "Votes to Delete: #{to_delete}"
            log "Votes Deleted: #{deleted}"
            total_votes_to_delete += to_delete
            total_votes_deleted += deleted
          else
            log "No voting issues found!"
          end
        end

        date += 1.day
      end

      log ""
      log "Summary:"
      log "  Pageant: #{pageant.name}"
      log "  Schedule: #{pageant.voting_start.to_date} to #{pageant.voting_end.to_date}"
      log "  Total Votes: #{total_votes}"
      log "  Votes to Delete: #{total_votes_to_delete}"
      log "  Votes Deleted: #{total_votes_deleted}" if total_votes_deleted > 0
    end
  end

end
