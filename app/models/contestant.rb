class Contestant < ActiveRecord::Base
  enum gender: [:female, :male]
  enum status: [:active, :deleted]

  after_create :record_activity

  belongs_to :pageant
  has_many :photos, dependent: :destroy
  has_many :votes, dependent: :destroy

  validates :number, :name, presence: true
  validates_uniqueness_of :number, conditions: -> { where(status: 0) }, scope: [:pageant_id, :gender, :status]

  scope :sort_by_number, ->{order("number ASC")}
  scope :not_deleted, ->{where.not(status: Contestant.statuses[:deleted])}
  accepts_nested_attributes_for :photos, :reject_if => :check_photos

  def check_photos(photo_attr)
    # Delete photo attribute in params when null
    return true if photo_attr['image'].nil?
    # Delete photo attribute in params when reached the limit
    return true if self.photos.count > 5
    return false

    #errors.add(:base, "You've reached the maximum upload photos.") if self.photos.count > 5
  end

  def photo
    photos.present? ? photos.first.image : nil
  end

  def sname
    "#{name} #{'-' if description.present?} #{description}".squeeze(" ").strip
  end

  def get_latest_vote(user)
    offset = user.vote_per_hour - 1
    contestants = pageant.male_and_female? ? pageant.contestants.send(gender) : pageant.contestants

    if contestants.present?
      return Vote.where(user: user, contestant_id: contestants.pluck(:id)).order(:created_at).offset(offset).last
    else
      return nil
    end
  end

  def add_boost_vote(request, user, bulk_vote)
    @user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT']) if request.present?
    last_vote = self.get_latest_vote(user)
    self.votes.create(
      user_id: user.id,
      bought: true,
      quantity: bulk_vote.quantity,
      ip_address: @user_agent.present? ? request.remote_ip : "0.0.0.0",
      platform: @user_agent.present? ? @user_agent.platform : "PayPal",
      browser: @user_agent.present? ? "#{@user_agent.browser} v#{@user_agent.version}" : nil
    )
    self.pageant.user.wallet.add_amount(bulk_vote.commission)
    return true
  end

  def add_vote(request, user, force = false, bought = false)
    @user_agent = UserAgent.parse(request.env['HTTP_USER_AGENT']) if request.present?
    last_vote = self.get_latest_vote(user)
    create_vote = false

    if force
      self.votes.create(
        user_id: user.id,
        bought: bought,
        ip_address: @user_agent.present? ? request.remote_ip : "0.0.0.0",
        platform: @user_agent.present? ? @user_agent.platform : "PayPal",
        browser: @user_agent.present? ? "#{@user_agent.browser} v#{@user_agent.version}" : nil
      )
      self.pageant.user.wallet.add_amount(bought ? Vote::BLK_PRICE : Vote::REG_PRICE)
      return true
    else
      remaining_votes = self.pageant.available_votes_of_user(user, gender)
      if remaining_votes > 0
        self.votes.create(
          user: user,
          bought: bought,
          ip_address: @user_agent.present? ? request.remote_ip : "0.0.0.0",
          platform: @user_agent.present? ? @user_agent.platform : "PayPal",
          browser: @user_agent.present? ? "#{@user_agent.browser} v#{@user_agent.version}" : nil
        )
        self.pageant.user.wallet.add_amount(bought ? Vote::BLK_PRICE : Vote::REG_PRICE)
        return true
      else
        return false
      end
    end
  end

  def vote_warning(user, new_line = false)
    if user.premium?
      if pageant.male_and_female?
        msg = "You have already casted all your votes for #{gender} contestants."
      else
        msg = "You have already casted all your votes."
      end
    else
      msg = "You have already casted your vote."
    end
    msg += new_line ? "<br>" : " "
    msg += "You can vote again in this pageant tomorrow."
    unless user.premium?
      msg += "<br><a class='premium-button' href='/info/premium-membership'>Click here</a> "
      msg += "to be a premium user and get 5 votes a day."
    end
    return msg
  end

  def record_activity
    Activity.create(user: pageant.user, resource: self, created_at: created_at, updated_at: updated_at, action: 'created')
  end
end
