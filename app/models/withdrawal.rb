class Withdrawal < ActiveRecord::Base

  enum status: [:pending, :completed, :approved, :rejected]
  
  validates :amount, presence: true
  
  belongs_to :user
  
  def self.valid_amount?(str)
    !!Float(str) rescue false
  end
  
  def status_style
    return "label-warning" if pending?
    return "label-primary" if approved?
    return "label-danger" if rejected?
    return "label-success" if completed?
    "label-default"
  end
  
  def help
    if pending?
      "Please wait for the administrator to review your withdrawal request."
    elsif approved?
      "Your request have been approved and the administrator is now processing the transfer."
    elsif rejected?
      notes
    end
  end
end
