class PremiumTransaction < ActiveRecord::Base
  enum status: [:pending, :completed, :failed]

  after_create :record_activity

  belongs_to :premium_denomination
  belongs_to :user
  has_one :bulk_vote

  attr_reader :redirect_uri, :popup_uri
  def setup!(return_url, cancel_url)
    # begin
      response = client.setup(
        payment_request,
        return_url,
        cancel_url,
        pay_on_paypal: true,
        no_shipping: true,
        allow_note: false
      )
      self.update_attributes(token: response.token)
      @redirect_uri = response.redirect_uri
      @popup_uri = response.popup_uri
      self
    # rescue
    #   return false
    # end
  end

  def complete!(payer_id = nil, force = false)
    success = true
    if force
      self.update_attributes(payer_id: payer_id, completed_at: Time.now)
      self.completed!
    else
      begin
        response = client.checkout!(self.token, payer_id, payment_request)
        transaction_id = response.payment_info.first.transaction_id
        self.update_attributes(txn_id: transaction_id, payer_id: payer_id, completed_at: Time.now)
        self.completed!
      rescue
        self.update_attributes(completed_at: Time.now)
        self.failed!
        success = false
      end
    end

    if success
      if premium_denomination.bulk_vote?
        pageant = bulk_vote.pageant
        contestant = bulk_vote.contestant
        (1..bulk_vote.quantity).each do |x|
          contestant.add_vote(nil, user, true, true)
        end
        Activity.create(user: user, resource: bulk_vote, created_at: created_at, updated_at: updated_at)
      elsif premium_denomination.boost_vote?
        pageant = bulk_vote.pageant
        contestant = bulk_vote.contestant
        contestant.add_boost_vote(nil, user, bulk_vote)
        Activity.create(user: user, resource: bulk_vote, created_at: created_at, updated_at: updated_at)
      else
        if user.premium_until.present?
          user.update_attributes(premium_until: user.premium_until + premium_denomination.validity.day)
        else
          user.update_attributes(premium_until: Date.today + premium_denomination.validity.day)
        end
      end
    end
    success
  end

  private

  def client
    Paypal::Express::Request.new(
      username: ENV['PAYPAL_USER'],
      password: ENV['PAYPAL_PASS'],
      signature: ENV['PAYPAL_SIGN']
    )
  end

  def payment_request
    if bulk_vote.present? && bulk_vote.contestant.present?
      description = premium_denomination.special_description(quantity, bulk_vote.contestant.sname)
    else
      description = premium_denomination.special_description(quantity)
    end
    Paypal::Payment::Request.new(
      amount: total_price,
      currency_code: :USD,
      items: [{
        name: premium_denomination.name,
        description: description,
        amount: total_price,
        quantity: total_quantity
      }]
    )
  end

  private

  def total_price
    if premium_denomination.boost_vote?
      price
    else
      price * quantity
    end
  end

  def total_quantity
    if premium_denomination.boost_vote?
      1
    else
      quantity
    end
    
  end

  def record_activity
    Activity.create(user: user, resource: self, created_at: created_at, updated_at: updated_at)
  end

end
