class User < ActiveRecord::Base
  paginates_per 100

  enum gender: [:female, :male]
  enum role: [:user, :vip, :admin]

  after_create :record_activity

  after_initialize :set_default_role, :if => :new_record?

  has_many :authentications, dependent: :destroy
  has_many :pageants
  has_many :bulk_votes, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :premium_transactions, dependent: :destroy
  has_many :activities, dependent: :destroy
  has_many :withdrawals
  has_many :bonus, class_name: "Bonus"
  has_one :wallet

  scope :premium, -> { where("premium_until IS NOT NULL", Date.today) }
  scope :with_email, -> { where("email IS NOT NULL", Date.today) }
  scope :with_pageants, -> { where("pageants_count > 0") }
  scope :with_votes, -> { where("votes_count > 0") }

  def give_bonus(amount)
    return false unless Withdrawal.valid_amount?(amount)
    prepare_wallet

  end

  def total_profit
    profit = 0.00
    pageants.each do |pageant|
      profit += pageant.profit
    end
    profit
  end

  def unpaid_pageant_ids
    Contestant.active.where("contestants.pageant_id IN (?) AND contestants.vote_count IS NOT NULL", pageants.pluck(:id)).pluck(:pageant_id).uniq
  end

  def can_withdraw?
    !withdrawals.where(status: Withdrawal.statuses["pending"]).present?
  end

  def prepare_wallet
    self.build_wallet.save unless self.wallet.present?
  end

  def current_balance
    wallet.present? ? wallet.amount : 0
  end

  def total_withdrawal
    withdrawals.completed.sum(:amount)
  end

  def set_default_role
    if User.count == 0
      self.role ||= :admin
    else
      self.role ||= :user
    end
  end

  def fb_photo_url
    auth = authentications.where(provider: "facebook").first
    auth.present? && auth.uid.present? ? "http://graph.facebook.com/#{auth.uid}/picture?type=large" : nil
  end

  def fb_url
    auth = authentications.where(provider: "facebook").first
    auth.present? && auth.uid.present? ? "https://www.facebook.com/#{auth.uid}" : nil
  end

  def age
    now = Time.now.utc.to_date
    now.year - birth_date.year - (birth_date.to_date.change(:year => now.year) > now ? 1 : 0)
  end

  def self.create_with_omniauth(auth)
    create! do |user|
      if auth['uid'].present?
        authentication = Authentication.new(provider: auth['provider'], uid: auth['uid'])
        if authentication.save
          if auth['info']['email'].present?
            user = User.find_by(email: auth['info']['email'])
          else
            user = nil
          end
          user ||= User.new(auth: auth)
          user.email = auth['info']['email']
          user.first_name = auth['info']['first_name']
          user.last_name = auth['info']['last_name']
          if user.save
            authentication.update_attributes(user_id: user.id)
          else
            raise "Omniauth Error: User cannot be saved!"
          end
        else
          raise "Omniauth Error: Authentication cannot be saved!"
        end
      end
      return authentication
    end
  end

  def update_from_facebook(auth)
    info = auth['info']
    raw = auth['extra']['raw_info']
    self.auth = auth
    self.email = auth['info']['email']
    if info['location'].present?
      self.location = info['location']
    end
    if raw['name'].present?
      self.name = raw['name']
    end
    if raw['first_name'].present?
      self.first_name = raw['first_name']
    end
    if raw['last_name'].present?
      self.last_name = raw['last_name']
    end
    if raw['gender'].present?
      self.gender = raw['gender']
    end
    if raw['birthday'].present?
      self.birth_date = raw['birthday']
    end
    if raw['link'].present?
      self.url = raw['link']
    end
    self.save
  end

  def update_token(auth)
    credentials = auth['credentials']
    self.token = credentials['token']
    self.token_expires_at = Time.at(credentials['expires_at'])
    self.save
  end

  def name
    [first_name, last_name].join(" ").strip
  end

  def premium?
    return false unless premium_until.present?
    Date.today <= premium_until
  end

  def valid_token?
    return false unless token_expires_at.present?
    Time.now <= token_expires_at
  end

  def vote_per_hour
    premium? ? 5 : 1
  end

  def vote_per_day
    premium? ? 5 : 1
  end

  def record_activity
    Activity.create(user: self, resource: self, created_at: created_at, updated_at: updated_at, action: 'created')
  end

end
