class PremiumDenomination < ActiveRecord::Base
  scope :boost_votes, ->{where("code LIKE 'PV-BOOST%'").order(validity: :desc)}

  def boost_vote?
    code.index("PV-BOOST") == 0
  end

  def bulk_vote?
    code.index("PVP-BV") == 0
  end
  
  def special_description(qty = 1, name = nil)
    if code == "PVP-BV"
      name = name.present? ? "for #{name}" : ""
      "Vote#{'s' if qty > 1} #{name}".strip
    else
      description
    end
  end
end
