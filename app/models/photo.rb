class Photo < ActiveRecord::Base
  LIMIT = 5
  has_attached_file :image, :styles => {medium: "600x600>", regular: "290x400#", thumb: "250x250>", small: "145x200#"}
  validates_attachment_content_type :image, :content_type => /image/
  belongs_to :contestant

  after_validation :clean_errors
  def clean_errors
    self.errors.delete(:image)
  end
end
