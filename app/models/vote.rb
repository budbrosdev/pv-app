class Vote < ActiveRecord::Base
  REG_PRICE = 0.05
  BLK_PRICE = 3.00

  paginates_per 25

  after_create :record_activity

  belongs_to :contestant
  counter_culture :contestant, column_name: "votes_count", delta_column: "quantity"
  belongs_to :user
  counter_culture :user

  scope :single, ->{where(quantity: 1)}

  def self.sanitize(date, delete = false)
    Rails.logger.info "Fix votes for #{date.inspect}"
    offset = Time.now.strftime("%z").gsub("0","").gsub("-", "- INTERVAL ").gsub("+", "+ INTERVAL ")
    total_votes_to_delete = 0
    total_votes_deleted = 0

    user_ids = Vote.where("DATE(created_at #{offset} HOUR) = DATE(?)", date).pluck(:user_id)
    User.where(id: user_ids.uniq).each do |user|

      log "User##{user.id} - #{user.name} (#{user.premium? ? 'Premium' : 'Non-Premium'})"
      contestant_ids = user.votes.where("DATE(created_at #{offset} HOUR) = DATE(?)", date).pluck(:contestant_id)
      pageant_ids = Contestant.where(id: contestant_ids.uniq).pluck(:pageant_id).uniq
      log "    Voted in #{pageant_ids.count} pageant#{'s' if pageant_ids.count > 1}"
      log ""

      Pageant.where(id: pageant_ids).each do |pageant|
        log "    Pageant##{pageant.id} - #{pageant.name} (#{pageant.category.titleize})"

        if pageant.male_and_female?
          # male
          contestant_ids = pageant.contestants.male.pluck(:id)
          current_votes = Vote.where("user_id = ? AND contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", user.id, contestant_ids, date)

          if current_votes.count > 5 && current_votes.count > user.vote_per_day
            votes_to_delete = current_votes.offset(user.vote_per_day)
            activities_to_delete = Activity.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
            # wallet_logs_to_delete = WalletLog.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
            total_votes_to_delete += votes_to_delete.count

            log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} for male"
            log "       --- Should delete #{current_votes.count - user.vote_per_day} votes"
            log "       --- #{delete ? 'Deleted' : 'Will delete'} #{votes_to_delete.count} votes"
            log "       --- #{delete ? 'Deleted' : 'Will delete'} #{activities_to_delete.count} #{activities_to_delete.count > 1 ? 'activities' : 'activity'}"

            if delete && votes_to_delete.count > 0
              total_votes_deleted += votes_to_delete.count
              activities_to_delete.delete_all
              votes_to_delete.delete_all
            end
          else
            log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} --- No changes"
          end

          log ""
          log ""

          # female
          contestant_ids = pageant.contestants.female.pluck(:id)
          current_votes = Vote.where("user_id = ? AND contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", user.id, contestant_ids, date)

          if current_votes.count > 5 && current_votes.count > user.vote_per_day
            votes_to_delete = current_votes.offset(user.vote_per_day)
            activities_to_delete = Activity.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
            # wallet_logs_to_delete = WalletLog.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
            total_votes_to_delete += votes_to_delete.count

            log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} for female"
            log "       --- Should delete #{current_votes.count - user.vote_per_day} votes"
            log "       --- #{delete ? 'Deleted' : 'Will delete'} #{votes_to_delete.count} votes"
            log "       --- #{delete ? 'Deleted' : 'Will delete'} #{activities_to_delete.count} #{activities_to_delete.count > 1 ? 'activities' : 'activity'}"

            if delete && votes_to_delete.count > 0
              total_votes_deleted += votes_to_delete.count
              activities_to_delete.delete_all
              votes_to_delete.delete_all
            end
          else
            log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} --- No changes"
          end

          log ""
          log ""

        else
          contestant_ids = pageant.contestants.pluck(:id)
          current_votes = Vote.where("user_id = ? AND contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", user.id, contestant_ids, date)

          if current_votes.count > 5 && current_votes.count > user.vote_per_day
            votes_to_delete = current_votes.offset(user.vote_per_day)
            activities_to_delete = Activity.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
            # wallet_logs_to_delete = WalletLog.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
            total_votes_to_delete += votes_to_delete.count

            log "       #{current_votes.count} vote#{'s' if current_votes.count > 1}"
            log "       --- Should delete #{current_votes.count - user.vote_per_day} votes"
            log "       --- #{delete ? 'Deleted' : 'Will delete'} #{votes_to_delete.count} votes"
            log "       --- #{delete ? 'Deleted' : 'Will delete'} #{activities_to_delete.count} #{activities_to_delete.count > 1 ? 'activities' : 'activity'}"

            if delete && votes_to_delete.count > 0
              total_votes_deleted += votes_to_delete.count
              activities_to_delete.delete_all
              votes_to_delete.delete_all
            end
          else
            log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} --- No changes"
          end

          log ""
          log ""

        end
      end
    end
    log "Total votes to delete: #{total_votes_to_delete}"
    log "Total votes deleted: #{total_votes_deleted}"
    return true
  end

  def self.sanitize_pageant(pageant_id, date, delete = false)
    offset = Time.now.strftime("%z").gsub("0","").gsub("-", "- INTERVAL ").gsub("+", "+ INTERVAL ")
    total_votes_to_delete = 0
    total_votes_deleted = 0

    pageant = Pageant.find(pageant_id)
    contestant_ids = pageant.contestants.pluck(:id)

    user_ids = Vote.where("DATE(created_at #{offset} HOUR) = DATE(?) AND contestant_id IN (?)", date, contestant_ids).pluck(:user_id)
    User.where(id: user_ids.uniq).each do |user|
      # log "User##{user.id} - #{user.name} (#{user.premium? ? 'Premium' : 'Non-Premium'})"
      if pageant.male_and_female?
        # male
        contestant_ids = pageant.contestants.male.pluck(:id)
        current_votes = Vote.where("user_id = ? AND contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", user.id, contestant_ids, date)

        if current_votes.count > 5 && current_votes.count > user.vote_per_day
          votes_to_delete = current_votes.offset(user.vote_per_day)
          activities_to_delete = Activity.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
          # wallet_logs_to_delete = WalletLog.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
          total_votes_to_delete += votes_to_delete.count

          log "User##{user.id} - #{current_votes.count} vote#{'s' if current_votes.count > 1} for male"
          # log "       --- Should delete #{current_votes.count - user.vote_per_day} votes"
          log "       --- #{delete ? 'Deleted' : 'Will delete'} #{votes_to_delete.count} votes"
          # log "       --- #{delete ? 'Deleted' : 'Will delete'} #{activities_to_delete.count} #{activities_to_delete.count > 1 ? 'activities' : 'activity'}"

          if delete && votes_to_delete.count > 0
            total_votes_deleted += votes_to_delete.count
            activities_to_delete.delete_all
            votes_to_delete.delete_all
          end
        else
          # log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} --- No changes"
        end

        # log ""
        # log ""

        # female
        contestant_ids = pageant.contestants.female.pluck(:id)
        current_votes = Vote.where("user_id = ? AND contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", user.id, contestant_ids, date)

        if current_votes.count > 5 && current_votes.count > user.vote_per_day
          votes_to_delete = current_votes.offset(user.vote_per_day)
          activities_to_delete = Activity.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
          # wallet_logs_to_delete = WalletLog.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
          total_votes_to_delete += votes_to_delete.count

          log "User##{user.id} - #{current_votes.count} vote#{'s' if current_votes.count > 1} for female"
          # log "       --- Should delete #{current_votes.count - user.vote_per_day} votes"
          log "       --- #{delete ? 'Deleted' : 'Will delete'} #{votes_to_delete.count} votes"
          # log "       --- #{delete ? 'Deleted' : 'Will delete'} #{activities_to_delete.count} #{activities_to_delete.count > 1 ? 'activities' : 'activity'}"

          if delete && votes_to_delete.count > 0
            total_votes_deleted += votes_to_delete.count
            activities_to_delete.delete_all
            votes_to_delete.delete_all
          end
        else
          # log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} --- No changes"
        end

        # log ""

      else
        contestant_ids = pageant.contestants.pluck(:id)
        current_votes = Vote.where("user_id = ? AND contestant_id IN (?) AND DATE(created_at #{offset} HOUR) = DATE(?)", user.id, contestant_ids, date)

        if current_votes.count > 5 && current_votes.count > user.vote_per_day
          votes_to_delete = current_votes.offset(user.vote_per_day)
          activities_to_delete = Activity.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
          # wallet_logs_to_delete = WalletLog.where(resource_type: "Vote", resource_id: votes_to_delete.pluck(:id))
          total_votes_to_delete += votes_to_delete.count

          log "User##{user.id} - #{current_votes.count} vote#{'s' if current_votes.count > 1}"
          # log "       --- Should delete #{current_votes.count - user.vote_per_day} votes"
          log "       --- #{delete ? 'Deleted' : 'Will delete'} #{votes_to_delete.count} votes"
          # log "       --- #{delete ? 'Deleted' : 'Will delete'} #{activities_to_delete.count} #{activities_to_delete.count > 1 ? 'activities' : 'activity'}"

          if delete && votes_to_delete.count > 0
            total_votes_deleted += votes_to_delete.count
            activities_to_delete.delete_all
            votes_to_delete.delete_all
          end
        else
          # log "       #{current_votes.count} vote#{'s' if current_votes.count > 1} --- No changes"
        end

        # log ""

      end
    end
    return [total_votes_to_delete, total_votes_deleted]
  end

  private

  def self.log(msg)
    return puts msg if Rails.env.development?
    Rails.logger.info msg
  end

  def record_activity
    if platform != "PayPal"
      Activity.create(user: user, resource: self, created_at: created_at, updated_at: updated_at)
    end
  end
end
