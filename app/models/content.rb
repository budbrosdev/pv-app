class Content < ActiveRecord::Base
  enum status: [:disabled, :enabled]
  NO_PREVIEW = ["home-section", "sidebar-section", "side-images", "side-script"]
  default_scope ->{order(:position)}
end
