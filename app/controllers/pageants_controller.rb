class PageantsController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :current, :past, :all]
  before_filter :get_pageant_public_access, only: [:show]

  def index
    @pageants = current_user.pageants.not_deleted
    @q = @pageants.search(params[:q])
    @pageants = @q.result(distinct: true).page(params[:page])
  end

  def show
    if @pageant.published_status == "upcoming"
      @seconds_remaining = (@pageant.voting_start.to_time.in_time_zone(ENV['TIMEZONE']) - Time.now.in_time_zone(ENV['TIMEZONE'])).to_i
    else
      @seconds_remaining = (@pageant.voting_end.to_time.in_time_zone(ENV['TIMEZONE']) - Time.now.in_time_zone(ENV['TIMEZONE'])).to_i
    end
    @contestants = @pageant.contestants.not_deleted.sort_by_number
    if @pageant.male_and_female?
      @top_male = @pageant.top(5, "male")
      @top_female = @pageant.top(5, "female")
    else
      @top_contestants = @pageant.top(5)
    end
    @other_pageants = @pageant.other(4)
  end

  def new
    @pageant = current_user.pageants.new
  end

  def create
    @pageant = current_user.pageants.new(pageant_params)
    if @pageant.save
      current_user.prepare_wallet
      redirect_to pageant_path(@pageant), notice: "You have created new pageant."
    else
      render :new
    end
  end

  def edit
    get_pageant
  end

  def update
    get_pageant
    if @pageant.update_attributes(pageant_params)
      Activity.create(user: current_user, resource: @pageant, action: "updated")
      redirect_to @pageant, notice: "You have saved your changes."
    else
      render :edit
    end
  end

  def destroy
    get_pageant
    if @pageant.deleted!
      Activity.create(user: current_user, resource: @pageant, action: "deleted")
      redirect_to pageants_path, notice: "You have deleted a pageant."
    else
      redirect_to @pageant, notice: "Failed to deleate this pageant, please try again."
    end

  end

  def publish
    get_pageant
    if @pageant.published!
      Activity.create(user: current_user, resource: @pageant, action: "published")
      redirect_to @pageant, notice: "You have published this pageant."
    else
      redirect_to @pageant, notice: "Failed to publish this pageant, please try again."
    end
  end

  def unpublish
    get_pageant
    if @pageant.unpublished!
      Activity.create(user: current_user, resource: @pageant, action: "unpublished")
      redirect_to @pageant, notice: "You have unpublished this pageant."
    else
      redirect_to @pageant, notice: "Failed to unpublish this pageant, please try again."
    end
  end

  def current
    @pageants = Pageant.published.current.order(:voting_start)
    @q = @pageants.search(params[:q])
    @pageants = @q.result(distinct: true).page(params[:page])
  end

  def past
    @pageants = Pageant.published.past.order(voting_end: :desc)
    @q = @pageants.search(params[:q])
    @pageants = @q.result(distinct: true).page(params[:page])
  end

  def all
    @pageants = Pageant.published.order(:voting_start)
    @q = @pageants.search(params[:q])
    @pageants = @q.result(distinct: true).page(params[:page])
  end

  private

  def get_pageant
    if current_user.vip? || current_user.admin?
      @pageant = Pageant.find_by(id: params[:id])
    else
      @pageant = current_user.pageants.not_deleted.find_by(id: params[:id])
    end

    if @pageant.nil?
      flash[:error] = "Pageant not found!"
      redirect_to current_pageants_path and return
    end
  end

  def get_pageant_public_access
    @pageant = Pageant.not_deleted.find_by(id: params[:id]) # Allow everyone to vote

    if @pageant.nil?
      flash[:error] = "Pageant not found!"
      redirect_to current_pageants_path and return
    elsif !@pageant.published? && (!user_signed_in? || !@pageant.authorized?(current_user))
      redirect_to current_pageants_path and return
    end
  end

  def pageant_params
    params.require(:pageant).permit(:name, :description, :category, :voting_start, :voting_end, :show_votes, :boost_votes, :header, :banner, :thumb, :fb_button)
  end
end
