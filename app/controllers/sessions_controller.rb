class SessionsController < ApplicationController

  def new
    session[:next_url] = params[:next_url]
    redirect_to '/auth/facebook'
  end

  def create
    auth = request.env["omniauth.auth"]
    authentication = Authentication.where(provider: auth['provider'], uid: auth['uid'].to_s).first || User.create_with_omniauth(auth)
    # raise authentication.inspect
    user = authentication.user
    if user.present?
      user.update_from_facebook(auth)
      user.update_token(auth) unless user.valid_token?
      next_url = session[:next_url]
      reset_session
      session[:user_id] = user.id
      if next_url.present?
        redirect_to next_url
      else
        redirect_to root_url, notice: "Welcome #{current_user.name}!"
      end
    else
      redirect_to root_url, notice: "Login failed! Please try again."
    end
  end

  def destroy
    reset_session
    redirect_to root_url, :notice => 'You have successfully logged out!'
  end

  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].try(:humanize)}"
  end

end
