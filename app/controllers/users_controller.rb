class UsersController < ApplicationController
  include ActionView::Helpers::NumberHelper
  before_filter :authenticate_user!, except: [:go_premium]
  before_filter :authenticate_admin!, except: [:show, :account, :edit, :update, :go_premium, :paypal_success, :paypal_failed, :buy_votes]
  before_filter :get_user, except: [:go_premium, :index, :paypal_success, :paypal_failed, :buy_votes]

  def index
    if params[:q].present? && params[:q][:first_name_or_last_name_or_email_or_location_cont].present?
      @keyword = params[:q][:first_name_or_last_name_or_email_or_location_cont].downcase
    end
    @remove_keyword = true
    if @keyword == "admin"
      @users = User.admin
    elsif @keyword == "user"
      @users = User.user
    elsif @keyword == "premium"
      @users = User.premium
    elsif @keyword == "email"
      @users = User.with_email
    elsif @keyword == "votes"
      @users = User.with_votes
    elsif @keyword == "pageants"
      @users = User.with_pageants
    else
      @users = User.all
      @remove_keyword = false
    end

    if @remove_keyword && @keyword.present?
      params[:q][:first_name_or_last_name_or_email_or_location_cont] = ""
      @q = @users.search(params[:q])
      params[:q][:first_name_or_last_name_or_email_or_location_cont] = @keyword
    else
      @q = @users.search(params[:q])
    end
    @users = @q.result(distinct: true).page(params[:page]).includes(:wallet)
  end

  def show
    @activities = @user.activities.order("activities.created_at DESC").page(params[:page])
  end

  def account
    @pageants = @user.pageants.sort_start
    @withdrawals = @user.withdrawals
    @bonus = @user.bonus
  end

  def give_bonus
    @user.prepare_wallet
    if @user.wallet.add_bonus(params[:bonus][:amount], params[:bonus][:notes])
      redirect_to account_user_path(@user), notice: "You have added <b>#{number_to_currency(params[:bonus][:amount], unit: 'Php ')}</b> worth of bonus credits to #{@user.name}"
    else
      redirect_to :back, alert: "Failed to give bonus credits. Please try again."
    end
  end

  def edit
    redirect_to root_path if current_user.user? && current_user != @user
  end

  def block
    @user.update_attributes(blocked: true)
    redirect_to :back, notice: "You have blocked #{@user.name}."
  end

  def unblock
    @user.update_attributes(blocked: false)
    redirect_to :back, notice: "You have unblocked #{@user.name}."
  end

  def update
    redirect_to root_path if current_user.user? && current_user != @user
    if @user.update_attributes(user_params)
      if params[:id] == "me"
        if user_params[:paypal_email].present?
          redirect_to account_user_path("me"), notice: "You have saved your Paypal email."
        else
          redirect_to user_path("me"), notice: "You have saved your changes."
        end
      else
        if user_params[:paypal_email].present?
          redirect_to account_user_path(@user), notice: "You have saved the user's PayPal email."
        else
          redirect_to user_path(@user), notice: "You have saved your changes."
        end
      end
    else
      render :edit
    end
  end

  def register_premium
    @transaction = @user.premium_transactions.new
  end

  def go_premium
    unless current_user.present?
      redirect_to root_path, alert: "You should login to avail premium membership!"
    else
      premium = PremiumDenomination.find_by(code: params[:code])
      if premium.present?
        transaction = current_user.premium_transactions.create(premium_denomination: premium, price: premium.price)
        if transaction.setup!(paypal_success_url, paypal_failed_url)
          redirect_to transaction.redirect_uri
        else
          transaction.failed!
          redirect_to :back, alert: "Transaction failed. Please try again later."
        end
      else
        redirect_to :back
      end
    end
  end

  def withdraw
    amount = params[:withdrawal][:amount]
    if !@user.paypal_email.present?
      redirect_to account_user_path(@user), alert: "Sorry, you need to setup your Payment Settings to request for withdrawal."
    elsif !amount.present?
      redirect_to account_user_path(@user), alert: "Please entry the amount to request for withdrawal."
    elsif !Withdrawal.valid_amount?(amount)
      redirect_to account_user_path(@user), alert: "Sorry, \"<b>#{amount}</b>\" is an invalid withdrawal amount. Please try again."
    elsif amount.to_f > @user.current_balance
      redirect_to account_user_path(@user), alert: "Sorry, insufficient funds. You cannot withdraw <b>#{number_to_currency(amount, unit: 'Php ')}</b> because you only have <b>#{number_to_currency(@user.current_balance, unit: 'Php ')}</b> in your account."
    elsif amount.to_f < 100
      redirect_to account_user_path(@user), alert: "You cannot withdraw lower than Php 100.00, please try again later."
    elsif !@user.can_withdraw?
      redirect_to account_user_path(@user), alert: "You currently have a pending withdrawal request. Please wait until we process your request. Thank you!"
    else
      @withdrawal = @user.withdrawals.create(amount: amount)
      AdminMailer.new_withdrawal(@withdrawal).deliver!
      redirect_to account_user_path(@user), notice: "You have submitted your request to withdraw  <b>#{number_to_currency(amount, unit: 'Php ')}</b> from your account.<br/>Please wait until we completely process your previous request."
    end
  end

  def buy_votes
    redirect_to info_path("buy-a-vote") if params[:bulk_vote].present? && params[:bulk_vote][:quantity] == 0
    vote_denomination = PremiumDenomination.find_by(code: "PVP-BV")
    bulk_vote = current_user.bulk_votes.new(bulk_vote_params)
    transaction = current_user.premium_transactions.create(premium_denomination: vote_denomination, price: vote_denomination.price, quantity: bulk_vote.quantity)
    bulk_vote.premium_transaction = transaction
    if bulk_vote.save
      if transaction.setup!(paypal_success_url, paypal_failed_url)
        redirect_to transaction.redirect_uri
      else
        transaction.failed!
        redirect_to :back, alert: "Transaction failed. Please try again later."
      end
    else
      transaction.failed!
    end
  end

  def paypal_success
    transaction = PremiumTransaction.find_by(token: params[:token])
    if transaction.present?
      if transaction.complete!(params[:PayerID])
        if transaction.premium_denomination.boost_vote?
          redirect_to pageant_contestant_path(transaction.bulk_vote.pageant_id, transaction.bulk_vote.contestant_id), notice: "Payment completed! Your boost votes are now counted."
        else
          redirect_to user_path("me"), notice: "Payment completed! You're now a premium user."
        end
      else
        if transaction.premium_denomination.boost_vote?
          redirect_to pageant_contestant_path(transaction.bulk_vote.pageant_id, transaction.bulk_vote.contestant_id), alert: "Payment failed! Please try again."
        else
          redirect_to user_path("me"), alert: "Payment failed! Please try again."
        end
      end
    else
      if transaction.premium_denomination.boost_vote?
        redirect_to pageant_contestant_path(transaction.bulk_vote.pageant_id, transaction.bulk_vote.contestant_id), alert: "Payment failed! Please contact our support for assistance."
      else
        redirect_to user_path("me"), alert: "Payment failed! Please contact our support for assistance."
      end
      redirect_to user_path("me"), alert: "Payment failed! Please contact our support for assistance."
    end
  end

  def paypal_failed
    transaction = PremiumTransaction.find_by(token: params[:token])
    if transaction.present?
      transaction.update_attributes(completed_at: Time.now)
      transaction.failed!
      if transaction.premium_denomination.boost_vote?
        redirect_to pageant_contestant_path(transaction.bulk_vote.pageant_id, transaction.bulk_vote.contestant_id), alert: "Payment failed! Please try again."
      else
        redirect_to user_path("me"), alert: "Payment failed! Please try again."
      end
    else
      redirect_to root_path, alert: "Payment failed! Please try again."
    end
    
  end

  private

  def get_user
    redirect_to root_path and return if params[:id] == "go_premium"
    redirect_to root_path and return if params[:id] != "me" && !admin?
    @user = params[:id] == "me" ? current_user : User.find(params[:id])
  end

  def bulk_vote_params
    params.require(:bulk_vote).permit(:pageant_id, :contestant_id, :quantity)
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :gender, :birth_date, :location, :role, :paypal_email)
  end
end