class ContestantsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  before_filter :get_pageant, except: [:vote, :boost_vote, :show]
  before_filter :get_pageant_public_access, only: [:votes, :show]

  def show
    if @pageant.published_status == "upcoming"
      @seconds_remaining = (@pageant.voting_start.to_time.in_time_zone(ENV['TIMEZONE']) - Time.now.in_time_zone(ENV['TIMEZONE'])).to_i
    else
      @seconds_remaining = (@pageant.voting_end.to_time.in_time_zone(ENV['TIMEZONE']) - Time.now.in_time_zone(ENV['TIMEZONE'])).to_i
    end
    @contestants = @pageant.contestants.not_deleted.sort_by_number

    if @contestant.present?
      @top_contestants = @pageant.top(10, @contestant.gender)
      @other_pageants = @pageant.other(4)
      @remaining_votes = user_signed_in? ? @pageant.available_votes_of_user(current_user, @contestant.gender) : 0
    end

    @boost_votes = PremiumDenomination.boost_votes
  end

  def votes
    redirect_to pageant_contestant_path(@pageant, @contestant) and return unless admin?
    @votes = @contestant.votes.order("created_at DESC")
    @q = @votes.search(params[:q])
    @votes = @q.result(distinct: true).page(params[:page])
  end

  def new
    @contestant = @pageant.contestants.new
    @contestant.number = @pageant.contestants.not_deleted.count + 1
  end

  def create
    @contestant = @pageant.contestants.new(contestant_params)
    @contestant.gender = @pageant.default_gender unless @pageant.male_and_female?
    if @contestant.save
      redirect_to pageant_contestant_path(@pageant, @contestant), notice: "You have added new contestant."
    else
      render :new
    end
  end

  def edit
    get_contestant
  end

  def update
    get_contestant
    if @contestant.update_attributes(contestant_params)
      Activity.create(user: current_user, resource: @contestant, action: "updated")
      redirect_to pageant_contestant_path(@pageant, @contestant), notice: "You have saved your changes."
    else
      render :edit
    end
  end

  def destroy
    get_contestant
    if @contestant.deleted!
      @contestant.update_attributes(number: nil)
      Activity.create(user: current_user, resource: @contestant, action: "deleted")
      redirect_to pageant_path(@pageant), notice: "You have deleted a contestant."
    else
      redirect_to :back
    end
  end

  def vote
    get_pageant_public_access
    get_contestant
    redirect_to pageant_contestant_path(@pageant, @contestant) and return if current_user.blocked? || blocked_ip?
    vote = @contestant.add_vote(request, current_user)
    if vote == true
      redirect_to pageant_contestant_path(@pageant, @contestant, voted: true), notice: "Thank you for voting #{@contestant.name}."
    else
      msg = @contestant.vote_warning(current_user)
      redirect_to pageant_contestant_path(@pageant, @contestant), alert: msg
    end
  end

  def boost_vote
    get_pageant_public_access
    get_contestant
    redirect_to pageant_contestant_path(@pageant, @contestant) and return if current_user.blocked? || blocked_ip?
    boost_vote = PremiumDenomination.find_by(code: params[:boost_vote])
    bulk_vote = current_user.bulk_votes.new(pageant: @pageant, contestant: @contestant, quantity: boost_vote.validity)
    transaction = current_user.premium_transactions.create(premium_denomination: boost_vote, price: boost_vote.price, quantity: boost_vote.validity)
    bulk_vote.premium_transaction = transaction
    if bulk_vote.save
      if transaction.setup!(paypal_success_url, paypal_failed_url)
        redirect_to transaction.redirect_uri
      else
        transaction.failed!
        redirect_to :back, alert: "Transaction failed. Please try again later."
      end
    else
      transaction.failed!
    end
  end

  def upload
    get_contestant
    @photo = @contestant.photos.build
  end

  def upload_process
    get_contestant
    if @contestant.update(contestant_photos_params)
      redirect_to pageant_contestant_path(@pageant, @contestant), notice: "You have uploaded new photo#{'s' if contestant_photos_params["photos_attributes"].count > 1}."
    else
      redirect_to :back, alert: @contestant.errors.full_messages.join(", ")
    end
  end

  def delete_photo
    get_contestant
    if params[:photo_id].present?
      photo = @contestant.photos.find_by(id: params[:photo_id])
      if photo.present?
        photo.destroy
        msg = "You have deleted a photo."
      else
        msg = "Invalid photo, please try again later"
      end
    end
    redirect_to :back, alert: msg
  end

  private

  def get_pageant
    if current_user.vip? || current_user.admin?
      @pageant = Pageant.find_by(id: params[:pageant_id])
    else
      @pageant = current_user.pageants.not_deleted.find_by(id: params[:pageant_id])
    end

    if @pageant.nil?
      flash[:error] = "Pageant not found!"
      redirect_to current_pageants_path and return
    end
  end

  def get_pageant_public_access
    @pageant = Pageant.not_deleted.find_by(id: params[:pageant_id]) # Allow everyone to vote

    if @pageant.nil?
      flash[:error] = "Pageant not found!"
      redirect_to current_pageants_path and return
    elsif !@pageant.published? && (!user_signed_in? || !@pageant.authorized?(current_user))
      redirect_to current_pageants_path and return
    end
    
    get_contestant
  end

  def get_contestant
    if @pageant.present?
      @contestant = @pageant.contestants.not_deleted.find_by(id: params[:id])

      if @contestant.nil?
        flash[:error] = "Contestant not found!"
        redirect_to pageant_path(@pageant) and return
      end
    end
  end

  def contestant_params
    params.require(:contestant).permit(:number, :name, :description, :gender, :vote_count)
  end

  def contestant_photos_params
    params.require(:contestant).permit(:id, photos_attributes: [:image])
  end
end
