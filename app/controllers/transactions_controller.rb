class TransactionsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_admin!
  before_filter :get_user
  
  def create
    transaction = @user.premium_transactions.new(transaction_params)
    if transaction.save!
      transaction.complete!("MANUAL", true)
      redirect_to @user, notice: "The user is now a premium member."
    else
      redirect_to @user, alert: "Process failed! Please contact our support for assistance."
    end
  end
  
  private 
  
  def get_user
    @user = User.find(params[:user_id])
  end
  
  def transaction_params
    params.require(:premium_transaction).permit(:premium_denomination_id, :txn_id, :price)
  end
end
