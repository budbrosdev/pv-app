class SettingsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_admin!

  def edit
  end

  def update
    params[:settings].each do |key, value|
      config = Setting.where(key: key).first_or_create
      new_value = value.squeeze(" ").strip
      if value.index(",")
        new_value = new_value.split(",").reject(&:empty?).reject(&:blank?).collect(&:strip).join(", ")
      end
      config.update_attributes(value: new_value)
    end
    redirect_to :back, notice: "You have saved the changes."
  end
end
