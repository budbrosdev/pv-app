class WithdrawalsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_admin!
  
  def index
    @withdrawals = Withdrawal.order("created_at DESC")
    @q = @withdrawals.search(params[:q])
    @withdrawals = @q.result(distinct: true).page(params[:page])
  end
  
  def approve
    get_withdrawal
    if @withdrawal.pending?
      @withdrawal.approved!
      redirect_to :back, notice: "You have approved a requested withdrawal, please transfer the amount immediately."
    else
      redirect_to withdrawals_path
    end
  end
  
  def reject
    get_withdrawal_from_params
    if @withdrawal.pending?
      @withdrawal.rejected!
      note = params[:withdrawal][:notes].present? ? params[:withdrawal][:notes] : "The administrator cannot accept your request. Please try again."
      @withdrawal.update_attributes(notes: note)
      redirect_to :back, notice: "You have rejected a requested withdrawal."
    else
      redirect_to withdrawals_path
    end
  end
  
  def complete
    get_withdrawal_from_params
    if @withdrawal.approved?
      if !params[:withdrawal][:txn_id].present?
        redirect_to :back, alert: "You need to enter the PayPal transaction ID to complete the withdrawal transaction."
      elsif @withdrawal.amount > @withdrawal.user.current_balance
        @withdrawal.rejected!
        @withdrawal.update_attributes(notes: "You cannot withdraw an amount greater than your current balance.")
        redirect_to :back, alert: "The user cannot withdraw an amount greater than its current balance. The withdrawal request has been rejected automatically."
      else
        amount = -@withdrawal.amount.to_f
        note = params[:withdrawal][:notes].present? ? params[:withdrawal][:notes] : "The administrator completed your request."
        @withdrawal.update_attributes(notes: note, txn_id: params[:withdrawal][:txn_id])
        @withdrawal.user.wallet.add_amount(amount, note, @withdrawal)
        @withdrawal.completed!
        redirect_to :back, notice: "You have successfully completed a withdrawal request."
      end
    else
      redirect_to withdrawals_path
    end
  end
  
  private
  
  def get_withdrawal
    @withdrawal = Withdrawal.find(params[:id])
  end

  def get_withdrawal_from_params
    if params[:withdrawal].present? && params[:withdrawal][:id].present?
      @withdrawal = Withdrawal.find(params[:withdrawal][:id]) 
    end
    redirect_to withdrawals_path unless @withdrawal.present?
  end
end
