class ContentsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:upload_image]
  before_filter :authenticate_user!, except: [:show]
  before_filter :authenticate_admin!, except: [:show]
  before_filter :get_content, except: [:index, :upload_image]

  def index
    @contents = Content.all
  end

  def show
    redirect_to root_path if !admin? && @content.disabled?
  end

  def edit
  end

  def update
    if @content.update_attributes(content_params)
      redirect_to contents_path, notice: "You have saved your changes."
    else
      render :edit
    end
  end

  def upload_image
    wysiwyg_image = WysiwygImage.new(attachment: params[:image])
    if wysiwyg_image.save!
      render json: {link: wysiwyg_image.attachment.url(:original)}
    else
      render json: []
    end
  end

  private

  def get_content
    @content = Content.find_by(id: params[:id]) || Content.find_by(area: params[:id])
    redirect_to root_path, alert: "Sorry, you're not allowed to access this page." unless @content.present?
  end

  def content_params
    params.require(:content).permit(:title, :script, :status, :description)
  end
end
