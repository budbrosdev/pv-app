class AdminMailer < ActionMailer::Base
  default from: "\"#{ENV['APP_NAME']}\" <wecare@budbrosdev.com>"

  def new_withdrawal(withdrawal)
    @recipients = User.where(role: [User.roles['admin'], User.roles['vip']]).collect{|x| "\"#{x.name}\" <#{x.email}>"}
    @withdrawal = withdrawal
    mail(to: @recipients.join(", "), subject: "[PageantVote] Withdrawal Request")
  end
end
